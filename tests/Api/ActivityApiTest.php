<?php

use App\Records\Activity\Activity;
use App\Records\Activity\ActivityId;
use App\Records\Activity\ActivityRepository;
use App\Records\Client\Client;
use App\Records\Client\ClientRepository;
use App\Records\Pet\Pet;
use App\Records\Pet\PetId;
use App\Records\Pet\PetRepository;
use App\Records\Pet\Sex;

class ActivityApiTest extends AbstractApiTestCase
{
    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var string
     */
    private $petId;

    public function setUp(): void
    {
        parent::setUp();

        $clientRepository = $this->app->make(ClientRepository::class);

        $client = new Client(
            $clientRepository->nextIdentity(),
            'Finn Mertens',
            'Land of Ooo',
            '12345'
        );

        $clientRepository->save($client);

        $petRepository = $this->app->make(PetRepository::class);

        $pet = new Pet(
            $petRepository->nextIdentity(),
            $client->id(),
            'Jake',
            'Dog',
            'Pug',
            Sex::male(),
            date_create_immutable()
        );

        $petRepository->save($pet);

        $this->petId = $pet->id()->value();

        $this->activityRepository = $this->app->make(ActivityRepository::class);
    }

    protected function baseUrl()
    {
        return '/activities';
    }

    public function testShouldUpdateActivity()
    {
        // Arrange
        $activityId = $this->createActivity()->id();

        // Act
        $updatedActivity = [
            'date' => '2019-03-25',
            'subject' => 'Edited Subject',
            'activity' => 'Edited Activity'
        ];

        $response = $this->json('PUT', '/' . $activityId->value(), $updatedActivity);

        // Assert
        $response->seeStatusCode(204);

        $savedActivity = $this->activityRepository->getById($activityId);

        self::assertEquals($updatedActivity['date'], $savedActivity->date()->format('Y-m-d'));
        self::assertEquals($updatedActivity['subject'], $savedActivity->subject());
        self::assertEquals($updatedActivity['activity'], $savedActivity->activity());
    }

    public function testShouldDeleteActivity()
    {
        // Arrange
        $activityId = $this->createActivity()->id();

        // Act
        $response = $this->json('DELETE', '/' . $activityId->value());

        // Assert
        $response->seeStatusCode(204);

        self::assertNull($this->activityRepository->getById($activityId));
    }

    private function createActivity()
    {
        $activity = new Activity(
            $this->activityRepository->nextIdentity(),
            new PetId($this->petId),
            DateTimeImmutable::createFromFormat('Y-m-d', '2019-03-26'),
            'Just checking',
            'We did this and that'
        );

        $this->activityRepository->save($activity);

        return $activity;
    }
}
