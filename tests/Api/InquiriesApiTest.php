<?php

use App\Inquiry\Inquiry;
use App\Inquiry\InquiryId;
use App\Inquiry\InquiryRepository;
use ReCaptcha\ReCaptcha;
use ReCaptcha\Response;

class InquiriesApiTest extends AbstractApiTestCase
{
    /**
     * @var InquiryRepository
     */
    private $inquiryRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->inquiryRepository = $this->app->make(InquiryRepository::class);

        $this->app->bind(ReCaptcha::class, function ($app) {
            $mockRecaptcha = $this->createMock(ReCaptcha::class);
            $mockResponse = $this->createMock(Response::class);

            $mockResponse->method('isSuccess')->willReturn(true);
            $mockRecaptcha->method('verify')->willReturn($mockResponse);

            return $mockRecaptcha;
        });
    }

    protected function baseUrl()
    {
        return '/inquiries';
    }

    public function testShouldGetAllInquiries()
    {
        // Arrange
        $inquiry1 = $this->createReadInquiry();
        $inquiry2 = $this->createUnreadInquiry();

        // Act
        $response = $this->json('GET', '');

        // Assert
        $response->seeJsonEquals([
            [
                'id' => $inquiry1->id()->value(),
                'sender' => $inquiry1->sender(),
                'message' => $inquiry1->message(),
                'date' => $inquiry1->date()->format(DATE_ATOM),
                'isRead' => $inquiry1->isRead()
            ],
            [
                'id' => $inquiry2->id()->value(),
                'sender' => $inquiry2->sender(),
                'message' => $inquiry2->message(),
                'date' => $inquiry2->date()->format(DATE_ATOM),
                'isRead' => $inquiry2->isRead()
            ]
        ]);
    }

    public function testShouldCreateInquiry()
    {
        // Arrange
        $inquiry = [
            'sender' => 'test@gmail.com',
            'message' => 'Hi, test message'
        ];

        // Act
        $response = $this->json('POST', '', $inquiry);

        // Assert
        $id = $response->response->getOriginalContent()['id'];

        self::assertNotNull($this->inquiryRepository->getById(new InquiryId($id)));
    }

    public function testShouldMarkInquiryAsRead()
    {
        // Arrange
        $inquiryId = $this->createUnreadInquiry()->id();

        // Act
        $response = $this->json('PUT', '/' . $inquiryId->value() . '/read');

        // Assert
        $response->seeStatusCode(204);

        $savedInquiry = $this->inquiryRepository->getById($inquiryId);

        self::assertTrue($savedInquiry->isRead());
    }

    public function testShouldMarkInquiryAsUnread()
    {
        // Arrange
        $inquiryId = $this->createReadInquiry()->id();
        // Act
        $response = $this->json('PUT', '/' . $inquiryId->value() . '/unread');

        // Assert
        $response->seeStatusCode(204);

        $savedInquiry = $this->inquiryRepository->getById($inquiryId);

        self::assertFalse($savedInquiry->isRead());
    }

    private function createUnreadInquiry()
    {
        $inquiry = new Inquiry(
            $this->inquiryRepository->nextIdentity(),
            'someone@email.com',
            'Test message'
        );

        $this->inquiryRepository->save($inquiry);

        return $inquiry;
    }

    private function createReadInquiry()
    {
        $inquiry = new Inquiry(
            $this->inquiryRepository->nextIdentity(),
            '123456789',
            'Another test message'
        );

        $inquiry->markAsRead();

        $this->inquiryRepository->save($inquiry);

        return $inquiry;
    }
}
