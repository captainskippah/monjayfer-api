<?php

use App\Auth\User;
use App\Records\Client\Client;
use App\Records\Client\ClientRepository;
use App\Records\Pet\Pet;
use App\Records\Pet\PetRepository;
use App\Records\Pet\Sex;
use Captainskippah\Scheduler\Domain\AppointmentId;
use Captainskippah\Scheduler\Domain\Attendant\AttendantId;
use Captainskippah\Scheduler\Domain\Attendee\Attendee;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeId;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeRepository;
use Captainskippah\Scheduler\Domain\DayRepository;
use Captainskippah\Scheduler\Domain\Schedule;
use Captainskippah\Scheduler\Domain\Time;
use Ramsey\Uuid\Uuid;

class CalendarApiTest extends AbstractApiTestCase
{
    /**
     * @var DayRepository
     */
    private $dayRepository;

    /**
     * @var string
     */
    private $attendeeId;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupTestAttendee();
        $this->dayRepository = $this->app->make(DayRepository::class);
    }

    protected function baseUrl()
    {
        return '/calendar';
    }

    public function testShouldReturnAppointmentsOfMonth()
    {
        // Arrange
        $date = DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-06');

        $day = $this->dayRepository->getDay($date);

        $day->bookAppointment(
            new AttendeeId($this->attendeeId),
            new AttendantId($this->attendantId),
            new Schedule(
                new Time(10, 0),
                new Time(11, 0)
            ),
            'Test appointment'
        );

        $this->dayRepository->save($day);

        // Act
        $response = $this->json('GET', '/2019/01');

        // Assert
        self::assertCount(1, $response->response->getOriginalContent());
    }

    public function testShouldBookAppointment()
    {
        // Arrange
        $appointment = [
            'petId' => $this->attendeeId,
            'start' => '12:00',
            'end' => '13:00',
            'notes' => 'monthly vaccination',
        ];

        // Act
        $response = $this->json('POST', '/2019/01/06/appointments', $appointment);

        // Assert
        $id = $response->response->getOriginalContent()['id'];
        $date = DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-06');
        $savedAppointment = $this->dayRepository->getDay($date)->appointment(new AppointmentId($id));

        self::assertNotNull($savedAppointment);
    }

    private function setupTestAttendee()
    {
        $clientRepository = $this->app->make(ClientRepository::class);
        $petRepository = $this->app->make(PetRepository::class);
        $attendeeRepository = $this->app->make(AttendeeRepository::class);

        $client = new Client(
            $clientRepository->nextIdentity(),
            'Finn Mertens',
            'Land of Ooo',
            '12345'
        );

        $pet = new Pet(
            $petRepository->nextIdentity(),
            $client->id(),
            'Jake',
            'Dog',
            'Pug',
            Sex::male(),
            DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-06')
        );

        $attendee = new Attendee(
            new AttendeeId($pet->id()->value()),
            $pet->name()
        );

        $clientRepository->save($client);
        $petRepository->save($pet);
        $attendeeRepository->save($attendee);

        $this->attendeeId = $pet->id()->value();
    }
}
