<?php

use App\Records\Activity\Activity;
use App\Records\Activity\ActivityId;
use App\Records\Activity\ActivityRepository;
use App\Records\Client\Client;
use App\Records\Client\ClientId;
use App\Records\Client\ClientRepository;
use App\Records\Pet\Pet;
use App\Records\Pet\PetApplicationService;
use App\Records\Pet\PetId;
use App\Records\Pet\PetQueryService;
use App\Records\Pet\PetRepository;
use App\Records\Pet\Sex;

class PetApiTest extends AbstractApiTestCase
{
    /**
     * @var PetApplicationService
     */
    private $petApplicationService;

    /**
     * @var PetQueryService
     */
    private $petQueryService;

    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var string
     */
    private $clientId;

    public function setUp(): void
    {
        parent::setUp();

        $this->petApplicationService = $this->app->make(PetApplicationService::class);
        $this->petQueryService = $this->app->make(PetQueryService::class);
        $this->activityRepository = $this->app->make(ActivityRepository::class);
        $clientRepository = $this->app->make(ClientRepository::class);

        $client = new Client(
            $clientRepository->nextIdentity(),
            'Homer Simpson',
            '742 Evergreen Terrace',
            '12345'
        );

        $clientRepository->save($client);

        $this->clientId = $client->id()->value();
    }

    protected function baseUrl()
    {
        return '/pets';
    }

    public function testShouldGetPetById()
    {
        // Arrange
        $petId = $this->createPet();

        // Act
        $response = $this->json('GET', '/' . $petId);

        // Assert
        $response->seeJsonEquals($this->petQueryService->getById($petId));
    }

    public function testShouldUpdatePet()
    {
        // Arrange
        $petId = $this->createPet();

        // Act
        $updatedPet = [
            'name' => 'Brian',
            'species' => 'Dog',
            'breed' => 'Labrador',
            'sex' => Sex::FEMALE,
            'birthday' => '2019-01-01'
        ];

        $response = $this->json('PUT', '/' . $petId, $updatedPet);

        // Assert
        $response->seeStatusCode(204);

        $updatedPet['id'] = $petId;

        $actual = $this->petQueryService->getById($petId);

        unset($actual['client']);

        self::assertEquals($updatedPet, $actual);
    }

    public function testShouldDeletePet()
    {
        // Arrange
        $petId = $this->createPet();

        // Act
        $response = $this->json('DELETE', '/' . $petId);

        // Assert
        $response->seeStatusCode(204);

        self::assertNull($this->petQueryService->getById($petId));
    }

    public function testShouldGetAllActivities()
    {
        // Arrange
        $petId = $this->createPet();

        $activity1 = $this->createActivity($petId);
        $activity2 = $this->createActivity2($petId);

        // Act
        $response = $this->json('GET', '/' . $petId . '/activities');

        // Assert
        $response->seeJsonEquals([
            [
                'id' => $activity1->id()->value(),
                'petId' => $petId,
                'date' => $activity1->date()->format('Y-m-d'),
                'subject' => $activity1->subject(),
                'activity' => $activity1->activity()
            ],
            [
                'id' => $activity2->id()->value(),
                'petId' => $petId,
                'date' => $activity2->date()->format('Y-m-d'),
                'subject' => $activity2->subject(),
                'activity' => $activity2->activity()
            ]
        ]);
    }

    public function testShouldCreateActivity()
    {
        // Arrange
        $petId = $this->createPet();

        // Act
        $activity = [
            'date' => date_create_immutable()->format('Y-m-d'),
            'subject' => 'Test subject',
            'activity' => 'Test activity'
        ];

        $response = $this->json('POST', '/' . $petId . '/activities', $activity);

        // Assert
        $id = $response->response->getOriginalContent()['id'];

        $savedActivity = $this->activityRepository->getById(new ActivityId($id));

        self::assertEquals($petId, $savedActivity->petId()->value());
        self::assertEquals($activity['date'], $savedActivity->date()->format('Y-m-d'));
        self::assertEquals($activity['subject'], $savedActivity->subject());
        self::assertEquals($activity['activity'], $savedActivity->activity());
    }

    private function createPet()
    {
        return $this->petApplicationService->create(
            $this->clientId,
            'Jake',
            'Dog',
            'Pug',
            Sex::MALE,
            DateTimeImmutable::createFromFormat(
                'Y-m-d',
                '2019-01-06'
            )
        );
    }

    private function createActivity(string $petId)
    {
        $activity = new Activity(
            $this->activityRepository->nextIdentity(),
            new PetId($petId),
            DateTimeImmutable::createFromFormat(
                'Y-m-d',
                '2019-03-26'
            ),
            'Leg Operation',
            'We did this and that'
        );

        $this->activityRepository->save($activity);

        return $activity;
    }

    private function createActivity2(string $petId)
    {
        $activity = new Activity(
            $this->activityRepository->nextIdentity(),
            new PetId($petId),
            DateTimeImmutable::createFromFormat(
                'Y-m-d',
                '2019-03-25'
            ),
            'General Checkup',
            'Findings is normal...'
        );

        $this->activityRepository->save($activity);

        return $activity;
    }
}
