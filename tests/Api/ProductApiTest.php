<?php

use App\Catalog\Product\Product as CatalogProduct;
use App\Catalog\Product\ProductId as CatalogProductId;
use App\Catalog\Product\ProductQueryService;
use App\Catalog\Product\ProductRepository as CatalogProductRepository;
use Captainskippah\Inventory\Domain\Product;
use Captainskippah\Inventory\Domain\ProductId as InventoryProductId;
use Captainskippah\Inventory\Domain\ProductRepository as InventoryProductRepository;
use Captainskippah\POS\Domain\Item\Item;
use Captainskippah\POS\Domain\Item\ItemId;
use Captainskippah\POS\Domain\Item\ItemRepository;

class ProductApiTest extends AbstractApiTestCase
{
    /**
     * @var InventoryProductRepository
     */
    private $inventoryProductRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var CatalogProductRepository
     */
    private $catalogProductRepository;

    /**
     * @var ProductQueryService
     */
    private $productQueryService;

    public function setUp(): void
    {
        parent::setUp();
        $this->inventoryProductRepository = $this->app->make(InventoryProductRepository::class);
        $this->itemRepository = $this->app->make(ItemRepository::class);
        $this->catalogProductRepository = $this->app->make(CatalogProductRepository::class);
        $this->productQueryService = $this->app->make(ProductQueryService::class);
    }

    protected function baseUrl()
    {
        return '/products';
    }

    public function testShouldCreateProduct()
    {
        // Arrange
        $product = [
            'name' => 'Whitening Soap',
            'stocks' => 10,
            'depletingQty' => 5,
            'price' => 100
        ];

        // Act
        $response = $this->json('POST', '/', $product);

        // Assert
        $id = $response->response->getOriginalContent()['id'];

        self::assertNotNull($this->inventoryProductRepository->getById(new InventoryProductId($id)));
        self::assertNotNull($this->itemRepository->getById(new ItemId($id)));
        self::assertNotNull($this->catalogProductRepository->getById(new CatalogProductId($id)));
    }

    public function testShouldGetAllProducts()
    {
        // Arrange
        $product1 = $this->createProduct();
        $product2 = $this->createProduct2();

        // Act
        $response = $this->json('GET', '/');

        // Assert
        $response->seeJsonEquals([
            $this->productQueryService->getById($product1->id()->value()),
            $this->productQueryService->getById($product2->id()->value())
        ]);
    }

    public function testShouldGetProductById()
    {
        // Arrange
        $product = $this->createProduct();

        // Act
        $response = $this->json('GET', '/' . $product->id()->value());

        // Assert
        $response->seeJsonEquals($this->productQueryService->getById($product->id()->value()));
    }

    public function testShouldUpdateProduct()
    {
        // Arrange
        $productId = $this->createProduct()->id()->value();

        $newProduct = [
            'name' => 'Whitening Soap 2',
            'depletingQty' => 10,
            'price' => 150
        ];

        // Act
        $response = $this->json('PUT', '/' . $productId, $newProduct);

        // Assert
        $response->assertResponseStatus(204);

        $savedInventoryProduct = $this->inventoryProductRepository->getById(new InventoryProductId($productId));
        self::assertEquals($newProduct['name'], $savedInventoryProduct->name());
        self::assertEquals($newProduct['depletingQty'], $savedInventoryProduct->depletingQty());

        $savedItem = $this->itemRepository->getById(new ItemId($productId));
        self::assertEquals($newProduct['name'], $savedItem->name());
        self::assertEquals($newProduct['price'], $savedItem->price());

        $savedCatalogProduct = $this->catalogProductRepository->getById(new CatalogProductId($productId));
        self::assertEquals($newProduct['name'], $savedCatalogProduct->name());
    }

    public function testShouldDeleteProduct()
    {
        // Arrange
        $productId = $this->createProduct()->id()->value();

        // Act
        $response = $this->json('DELETE', '/' . $productId);

        // Assert
        $response->assertResponseStatus(204);

        self::assertNull($this->inventoryProductRepository->getById(new InventoryProductId($productId)));
        self::assertNull($this->itemRepository->getById(new ItemId($productId)));
        self::assertNull($this->catalogProductRepository->getById(new CatalogProductId($productId)));
    }

    public function testShouldRestockProduct()
    {
        // Arrange
        $product = $this->createProduct();

        // Act
        $stocks = ['qty' => 50];

        $response = $this->json('POST', '/' . $product->id()->value() . '/restock', $stocks);

        // Assert
        $response->seeStatusCode(204);

        $savedInventory = $this->inventoryProductRepository->getById($product->id());
        self::assertEquals($product->stockQty() + 50, $savedInventory->stockQty());
    }

    public function testShouldTakeStocksFromProduct()
    {
        // Arrange
        $product = $this->createProduct();

        // Act
        $stocks = ['qty' => 10];

        $response = $this->json('POST', '/' . $product->id()->value() . '/consume', $stocks);

        // Assert
        $response->seeStatusCode(204);

        $savedInventory = $this->inventoryProductRepository->getById($product->id());
        self::assertEquals($product->stockQty() - 10, $savedInventory->stockQty());
    }

    private function createProduct()
    {
        $savedProduct = new Product(
            $this->inventoryProductRepository->nextIdentity(),
            "Whitening Soap Product",
            50,
            10
        );

        $this->inventoryProductRepository->save($savedProduct);

        $savedItem = new Item(
            new ItemId($savedProduct->id()->value()),
            $savedProduct->name(),
            100
        );

        $this->itemRepository->save($savedItem);

        $savedCatalogProduct = new CatalogProduct(
            new CatalogProductId($savedProduct->id()->value()),
            $savedProduct->name()
        );

        $this->catalogProductRepository->save($savedCatalogProduct);

        return $savedProduct;
    }

    private function createProduct2()
    {
        $savedProduct = new Product(
            $this->inventoryProductRepository->nextIdentity(),
            "Dog Food",
            30,
            15
        );

        $this->inventoryProductRepository->save($savedProduct);

        $savedItem = new Item(
            new ItemId($savedProduct->id()->value()),
            $savedProduct->name(),
            150
        );

        $this->itemRepository->save($savedItem);

        $savedCatalogProduct = new CatalogProduct(
            new CatalogProductId($savedProduct->id()->value()),
            $savedProduct->name()
        );

        $this->catalogProductRepository->save($savedCatalogProduct);

        return $savedProduct;
    }
}
