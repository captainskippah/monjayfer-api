<?php

use App\Auth\CustomerAccountGenerated;
use App\Auth\User;
use App\Records\Client\Client;
use App\Records\Client\ClientId;
use App\Records\Client\ClientRepository;
use App\Records\Pet\PetApplicationService;
use App\Records\Pet\PetId;
use App\Records\Pet\PetQueryService;
use App\Records\Pet\Sex;

class ClientApiTest extends AbstractApiTestCase
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var PetApplicationService
     */
    private $petApplicationService;

    /**
     * @var PetQueryService
     */
    private $petQueryService;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientRepository = $this->app->make(ClientRepository::class);
        $this->petApplicationService = $this->app->make(PetApplicationService::class);
        $this->petQueryService = $this->app->make(PetQueryService::class);
    }

    protected function baseUrl()
    {
        return '/clients';
    }

    public function testShouldGetAllClients()
    {
        // Arrange
        $client1 = $this->createClient1();
        $client2 = $this->createClient2();

        // Action
        $response = $this->json('GET', '/');

        // Assert
        $response->seeJsonEquals([
            [
                'id' => $client1->id()->value(),
                'name' => $client1->name(),
                'address' => $client1->address(),
                'phone' => $client1->phone(),
                'account' => false
            ],

            [
                'id' => $client2->id()->value(),
                'name' => $client2->name(),
                'address' => $client2->address(),
                'phone' => $client2->phone(),
                'account' => false

            ],
        ]);
    }

    public function testShouldGetClientById()
    {
        // Arrange
        $client = $this->createClient1();

        // Act
        $response = $this->json('GET', '/' . $client->id()->value());

        // Assert
        $response->seeJsonEquals([
            'id' => $client->id()->value(),
            'name' => $client->name(),
            'address' => $client->address(),
            'phone' => $client->phone(),
            'account' => false
        ]);
    }

    public function testShouldCreateClient()
    {
        // Arrange
        $client = [
            'name' => 'Homer Simpson',
            'address' => '742 Evergreen Terrace',
            'phone' => '12345'
        ];

        // Act
        $response = $this->json('POST', '/', $client);

        // Assert
        $response->seeStatusCode(201);
        $response->seeJson($client);

        $id = $response->response->getOriginalContent()['id'];

        $savedClient = $this->clientRepository->getById(new ClientId($id));

        self::assertEquals($client['name'], $savedClient->name());
        self::assertEquals($client['address'], $savedClient->address());
        self::assertEquals($client['phone'], $savedClient->phone());
    }

    public function testShouldUpdateClient()
    {
        // Arrange
        $client = $this->createClient1();

        // Act
        $updatedClient = [
            'name' => 'Bart Simpson',
            'address' => '742 Evergreen Terrace',
            'phone' => '98765'
        ];

        $response = $this->json('PUT', '/' . $client->id()->value(), $updatedClient);

        // Assert
        $response->seeStatusCode(204);

        $savedClient = $this->clientRepository->getById($client->id());

        self::assertEquals($updatedClient['name'], $savedClient->name());
        self::assertEquals($updatedClient['address'], $savedClient->address());
        self::assertEquals($updatedClient['phone'], $savedClient->phone());
    }

    public function testShouldDeleteClient()
    {
        // Arrange
        $client = $this->createClient1();

        // Act
        $response = $this->json('DELETE', '/' . $client->id()->value());

        // Assert
        $response->seeStatusCode(204);

        self::assertNull($this->clientRepository->getById($client->id()));
    }

    public function testShouldGetClientPets()
    {
        // Arrange
        $client = $this->createClient1();

        $this->createPet1($client->id());
        $this->createPet2($client->id());

        // Act
        $response = $this->json('GET', '/' . $client->id()->value() . '/pets');

        // Assert
        $response->seeJsonEquals($this->petQueryService->getByClientId($client->id()->value()));
    }

    public function testShouldCreateClientPets()
    {
        // Arrange
        $client = $this->createClient1();

        // Act
        $pet = [
            'name' => 'Santa\'s Little Helper',
            'species' => 'Dog',
            'breed' => 'Greyhound',
            'sex' => Sex::MALE,
            'birthday' => '2019-01-01'
        ];

        $response = $this->json('POST', '/' . $client->id()->value() . '/pets', $pet);

        // Assert
        $response->seeStatusCode(201);
        $response->seeJson($pet);

        $id = $response->response->getOriginalContent()['id'];

        self::assertNotNull($this->petQueryService->getById($id));
    }

    public function testShouldGenerateAccountForClient()
    {
        // Arrange
        $client = $this->createClient1();

        // Prevent handlers from running
        $this->withoutEvents();

        // Assert (expect)
        $this->expectsEvents(CustomerAccountGenerated::class);

        // Act
        $response = $this->json('POST', '/' . $client->id()->value() . '/generateAccount');

        // Assert
        $response->seeStatusCode(204);

        self::assertNotNull(User::find($client->id()->value()));
    }

    private function createClient1()
    {
        $client = new Client(
            $this->clientRepository->nextIdentity(),
            'Homer Simpson',
            '742 Evergreen Terrace',
            '12345'
        );

        $this->clientRepository->save($client);

        return $client;
    }

    private function createClient2()
    {
        $client = new Client(
            $this->clientRepository->nextIdentity(),
            'Bart Simpson',
            '742 Evergreen Terrace',
            '98765'
        );

        $this->clientRepository->save($client);

        return $client;
    }

    private function createPet1(ClientId $clientId)
    {
        return $this->petApplicationService->create(
            $clientId->value(),
            'Santa\'s Little Helper',
            'Dog',
            'Greyhound',
            Sex::MALE,
            DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-01')
        );
    }

    private function createPet2(ClientId $clientId)
    {
        return $this->petApplicationService->create(
            $clientId->value(),
            'Brian',
            'Dog',
            'Labrador',
            Sex::MALE,
            DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-01')
        );
    }
}
