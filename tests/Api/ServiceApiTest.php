<?php

use App\Catalog\Service\Service;
use App\Catalog\Service\ServiceId;
use App\Catalog\Service\ServiceRepository;
use Captainskippah\POS\Domain\Item\Item;
use Captainskippah\POS\Domain\Item\ItemId;
use Captainskippah\POS\Domain\Item\ItemRepository;

class ServiceApiTest extends AbstractApiTestCase
{
    /**
     * @var ServiceRepository
     */
    private $serviceRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->serviceRepository = $this->app->make(ServiceRepository::class);
        $this->itemRepository = $this->app->make(ItemRepository::class);
    }

    protected function baseUrl()
    {
        return '/services';
    }

    public function testShouldCreateService()
    {
        // Arrange
        $service = [
            'name' => 'Pet Grooming',
            'price' => '100.00'
        ];

        // Act
        $response = $this->json('POST', '/', $service);

        // Assert
        $id = $response->response->getOriginalContent()['id'];

        self::assertNotNull($this->serviceRepository->getById(new ServiceId($id)));
        self::assertNotNull($this->itemRepository->getById(new ItemId($id)));
    }

    public function testShouldGetAllServices()
    {
        // Arrange
        $service1 = [
            'name' => 'Car wash',
            'price' => '100.00'
        ];

        $service2 = [
            'name' => 'Paint job',
            'price' => '200.00'
        ];

        $service1['id'] = $this->saveService($service1);
        $service2['id'] = $this->saveService($service2);

        // Act
        $response = $this->json('GET', '/');

        // Assert
        $response->seeJsonEquals([$service1, $service2]);
    }

    public function testShouldGetServiceById()
    {
        // Arrange
        $service = [
            'name' => 'Pedicure',
            'price' => '100.00'
        ];

        $service['id'] = $this->saveService($service);

        // Act
        $response = $this->json('GET', '/' . $service['id']);

        // Assert
        $response->seeJsonEquals($service);
    }

    public function testShouldUpdateService()
    {
        // Arrange
        $service = [
            'name' => 'Version1',
            'price' => 100
        ];

        $service['id'] = $this->saveService($service);

        // Act
        $updatedService = [
            'name' => 'Version2',
            'price' => 200
        ];

        $response = $this->json('PUT', '/' . $service['id'], $updatedService);

        // Assert
        $response->seeStatusCode(204);

        $savedCatalogEntry = $this->serviceRepository->getById(new ServiceId($service['id']));

        self::assertEquals('Version2', $savedCatalogEntry->name());

        $savedItem = $this->itemRepository->getById(new ItemId($service['id']));

        self::assertEquals('Version2', $savedItem->name());
        self::assertEquals(200, $savedItem->price());
    }

    public function testShouldDeleteService()
    {
        // Arrange
        $serviceId = $this->saveService([
            'name' => 'Room cleaning',
            'price' => 100
        ]);

        // Act
        $this->json('DELETE', '/' . $serviceId);

        // Assert
        self::assertNull($this->serviceRepository->getById(new ServiceId($serviceId)));
        self::assertNull($this->itemRepository->getById(new ItemId($serviceId)));
    }

    private function saveService(array $service)
    {
        $catalogEntry = new Service(
            $this->serviceRepository->nextIdentity(),
            $service['name']
        );

        $this->serviceRepository->save($catalogEntry);

        $item = new Item(
            new ItemId($catalogEntry->id()->value()),
            $catalogEntry->name(),
            $service['price']
        );

        $this->itemRepository->save($item);

        return $catalogEntry->id()->value();
    }
}
