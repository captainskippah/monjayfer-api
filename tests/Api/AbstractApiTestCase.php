<?php

use App\Auth\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Ramsey\Uuid\Uuid;

abstract class AbstractApiTestCase extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var string
     */
    protected $attendantId;

    public function setUp(): void
    {
        parent::setUp();

        $this->disableExceptionHandling();

        $this->setupTestAttendant();
    }

    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $uri = $this->trimSlash('/api/' . $this->baseUrl() . $uri);

        return parent::call($method, $uri, $parameters, $cookies, $files, $server, $content);
    }

    protected function setupTestAttendant()
    {
        $user = User::createEmployeeAccount(
            Uuid::uuid4()->serialize(),
            'Main Admin',
            'admin',
            $this->app->make('hash')->make('password')
        );

        $this->attendantId = $user->id;

        $this->actingAs($user);
    }

    abstract protected function baseUrl();

    private function trimSlash(string $string)
    {
        return preg_replace('#/+#', '/', $string);
    }
}
