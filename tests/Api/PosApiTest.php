<?php

use App\POS\POSSettingsProvider;
use Captainskippah\Inventory\Domain\Product;
use Captainskippah\Inventory\Domain\ProductRepository;
use Captainskippah\POS\Domain\Cart\PosVatProvider;
use Captainskippah\POS\Domain\Item\Item;
use Captainskippah\POS\Domain\Item\ItemId;
use Captainskippah\POS\Domain\Item\ItemRepository;
use Captainskippah\POS\Domain\Purchase\PurchaseId;
use Captainskippah\POS\Domain\Purchase\PurchaseRepository;

class PosApiTest extends AbstractApiTestCase
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var PurchaseRepository
     */
    private $purchaseRepository;

    /**
     * @var PosVatProvider
     */
    private $posVatProvider;

    public function setUp(): void
    {
        parent::setUp();
        $this->productRepository = $this->app->make(ProductRepository::class);
        $this->itemRepository = $this->app->make(ItemRepository::class);
        $this->purchaseRepository = $this->app->make(PurchaseRepository::class);
        $this->posVatProvider = $this->app->make(POSSettingsProvider::class);
    }

    protected function baseUrl()
    {
        return '/pos';
    }

    public function testShouldCheckoutOrder()
    {
        // Arrange
        $product = new Product($this->productRepository->nextIdentity(), 'Dog Food', 10);
        $this->productRepository->save($product);

        $productPosItem = new Item(new ItemId($product->id()->value()), $product->name(), 100);
        $this->itemRepository->save($productPosItem);

        $servicePosItem = new Item($this->itemRepository->nextIdentity(), 'Grooming', 100);
        $this->itemRepository->save($servicePosItem);

        // Act
        $order = [
            'items' => [
                ['id' => $productPosItem->id()->value(), 'qty' => 2],
                ['id' => $servicePosItem->id()->value(), 'qty' => 1]
            ],

            'payment' => [
                'type' => 'cash',
                'amount' => 500
            ]
        ];

        // Arrange
        $response = $this->json('POST', '/checkout', $order);

        // Assert
        $response->assertResponseStatus(200);

        $responseBody = $response->response->getOriginalContent();

        self::assertNotNull($this->purchaseRepository->getById(new PurchaseId($responseBody['id'])));
    }

    public function testShouldReturnPosSettings()
    {
        // Act
        $request = $this->json('GET', '/settings');

        // Assert
        $request->assertResponseStatus(200);

        $request->seeJsonEquals([
            'vatRate' => 12
        ]);
    }
}
