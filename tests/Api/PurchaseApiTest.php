<?php

use App\POS\PurchaseTransformer;
use Captainskippah\POS\Application\CashPayment;
use Captainskippah\POS\Domain\Purchase\Purchase;
use Captainskippah\POS\Domain\Purchase\PurchaseItem;
use Captainskippah\POS\Domain\Purchase\PurchaseRepository;

class PurchaseApiTest extends AbstractApiTestCase
{
    /**
     * @var PurchaseRepository
     */
    private $purchaseRepository;

    /**
     * @var PurchaseTransformer
     */
    private $purchaseTransformer;

    public function setUp(): void
    {
        parent::setUp();
        $this->purchaseRepository = $this->app->make(PurchaseRepository::class);
        $this->purchaseTransformer = new PurchaseTransformer();
    }

    protected function baseUrl()
    {
        return '/purchases';
    }

    public function testShouldGetAllPurchase()
    {
        // Arrage
        $purchase1 = $this->createPurchase();
        $purchase2 = $this->createPurchase();

        // Act
        $response = $this->json('GET', '/');

        // Assert
        $response->seeStatusCode(200);
        $response->seeJsonEquals($this->purchaseTransformer->transform($purchase1, $purchase2));

        self::assertCount(2, $response->response->getOriginalContent());
    }

    public function testShouldGetPurchaseById()
    {
        // Arrage
        $purchase = $this->createPurchase();

        // Act
        $response = $this->json('GET', '/' . $purchase->id()->value());

        // Assert
        $response->seeStatusCode(200);
        $response->seeJsonEquals($this->purchaseTransformer->transform($purchase)[0]);
    }

    private function createPurchase()
    {
        $items = [
            new PurchaseItem("Oreo", 50, 1),
            new PurchaseItem("Milk", 25, 1)
        ];

        $purchase = new Purchase(
            $this->purchaseRepository->nextIdentity(),
            $items,
            12,
            new CashPayment(100)
        );

        $this->purchaseRepository->save($purchase);

        return $purchase;
    }
}
