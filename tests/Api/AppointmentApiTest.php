<?php

use App\Auth\User;
use App\Records\Client\Client;
use App\Records\Client\ClientRepository;
use App\Records\Pet\Pet;
use App\Records\Pet\PetApplicationService;
use App\Records\Pet\PetRepository;
use App\Records\Pet\Sex;
use App\Scheduler\AppointmentQueryService;
use Captainskippah\Scheduler\Domain\AppointmentId;
use Captainskippah\Scheduler\Domain\AppointmentStatus;
use Captainskippah\Scheduler\Domain\Attendant\AttendantId;
use Captainskippah\Scheduler\Domain\Attendee\Attendee;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeId;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeRepository;
use Captainskippah\Scheduler\Domain\DayRepository;
use Captainskippah\Scheduler\Domain\Schedule;
use Captainskippah\Scheduler\Domain\Time;
use Ramsey\Uuid\Uuid;

class AppointmentApiTest extends AbstractApiTestCase
{
    /**
     * @var string
     */
    private $attendeeId;

    /**
     * @var DateTimeImmutable
     */
    private $date;

    /**
     * @var DayRepository
     */
    private $dayRepository;

    /**
     * @var AppointmentQueryService
     */
    private $appointmentQueryService;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupTestAttendee();
        $this->date = DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-06');
        $this->dayRepository = $this->app->make(DayRepository::class);
        $this->appointmentQueryService = $this->app->make(AppointmentQueryService::class);
    }

    protected function baseUrl()
    {
        return '/appointments';
    }

    public function testShouldGetAppointment()
    {
        // Arrange
        $appointment = $this->saveAppointment();

        // Act
        $response = $this->json('GET', '/' . $appointment->id()->value());

        // Assert
        $id = $response->response->getOriginalContent()['id'];

        $expected = $this->appointmentQueryService->getById($id);
        $expected['date'] = $expected['date']->format('Y-m-d');

        $response->seeJsonEquals($expected);
    }

    public function testShouldDeleteAppointment()
    {
        // Arrange
        $appointmentId = $this->saveAppointment()->id()->value();

        // Act
        $response = $this->json('DELETE', '/' . $appointmentId);

        // Assert
        $response->seeStatusCode(204);

        self::assertNull($this->getAppointment($appointmentId));
    }

    public function testShouldCancelAppointment()
    {
        // Arrange
        $appointmentId = $this->saveAppointment()->id()->value();

        // Act
        $response = $this->json('PUT', '/' . $appointmentId . '/cancel');

        // Assert
        $response->seeStatusCode(204);

        $savedAppointment = $this->getAppointment($appointmentId);

        self::assertTrue(AppointmentStatus::cancelled()->equals($savedAppointment->status()));
    }

    public function testShouldMarkAppointmentAsDone()
    {
        // Arrange
        $appointmentId = $this->saveAppointment()->id()->value();

        // Act
        $response = $this->json('PUT', '/' . $appointmentId . '/done');

        // Assert
        $response->seeStatusCode(204);

        $savedAppointment = $this->getAppointment($appointmentId);

        self::assertTrue(AppointmentStatus::done()->equals($savedAppointment->status()));
    }

    public function testShouldRescheduleAppointment()
    {
        // Arrange
        $appointmentId = $this->saveAppointment(AppointmentStatus::CANCELLED)->id()->value();

        // Act
        $response = $this->json('PUT', '/' . $appointmentId . '/schedule');

        // Assert
        $response->seeStatusCode(204);

        $savedAppointment = $this->getAppointment($appointmentId);

        self::assertTrue(AppointmentStatus::scheduled()->equals($savedAppointment->status()));
    }

    private function setupTestAttendee()
    {
        $clientRepository = $this->app->make(ClientRepository::class);
        $petApplicationService = $this->app->make(PetApplicationService::class);

        $client = new Client(
            $clientRepository->nextIdentity(),
            'Finn Mertens',
            'Land of Ooo',
            '12345'
        );

        $clientRepository->save($client);

        $this->attendeeId = $petApplicationService->create(
            $client->id()->value(),
            'Jake',
            'Dog',
            'Pug',
            Sex::MALE,
            DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-06')
        );
    }

    private function saveAppointment(string $status = null)
    {
        $day = $this->dayRepository->getDay($this->date);

        $appointmentId = $day->bookAppointment(
            new AttendeeId($this->attendeeId),
            new AttendantId($this->attendantId),
            new Schedule(
                new Time(12, 0),
                new Time(13, 0)
            ),
            'Test Note'
        );

        if ($status !== null && $status !== AppointmentStatus::SCHEDULED) {
            $day->changeAppointmentStatus($appointmentId, new AppointmentStatus($status));
        }

        $this->dayRepository->save($day);

        return $day->appointment($appointmentId);
    }

    private function getAppointment(string $appointmentId)
    {
        $day = $this->dayRepository->getDay($this->date);

        return $day->appointment(new AppointmentId($appointmentId));
    }
}
