<?php

use App\Exceptions\Handler;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Contracts\Debug\ExceptionHandler;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        // Unlike Laravel, Lumen's Kernel bootstrap at constructor
        // therefore it does not need to explicitly call `bootstrap()` method
        $app->make(Kernel::class);

        return $app;
    }

    protected function disableExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct() {}

            public function report(Exception $exception) {}

            public function render($request, Exception $exception)
            {
                throw $exception;
            }
        });
    }

    protected function seeInDatabase($table, array $data, $onConnection = null)
    {
        $newData = [];

        // According to our style guide, database tables and columns are snake_case
        foreach ($data as $key => $value) {
            $newData[snake_case($key)] = $value;
        }

        return parent::seeInDatabase($table, $newData, $onConnection);
    }
}
