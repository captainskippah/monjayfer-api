<?php

namespace App\Scheduler;

use DateTimeImmutable;
use Infrastructure\AbstractLumenRepository;

class AppointmentQueryService extends AbstractLumenRepository
{
    public function getById(string $id): ?array
    {
        $queryResult =  $this->query()->where('t1.id', $id)->first();

        return $this->formatQueryResult($queryResult);
    }

    public function getByMonth(int $year, int $month): array
    {
        return $this->query()
            ->whereRaw('EXTRACT(YEAR FROM date) = ?', [$year])
            ->whereRaw('EXTRACT(MONTH FROM date) = ?', [$month])
            ->get()
            ->transform(function ($queryResult) {
                return $this->formatQueryResult($queryResult);
            })
            ->toArray();
    }

    private function query()
    {
        return $this->database
            ->table('scheduler_appointments as t1')
            ->join('scheduler_attendees as t2', 't2.id', '=', 't1.attendee_id')
            ->join('users as t3', 't3.id', '=', 't1.attendant_id')
            ->select([
                't1.id',
                't1.date',

                't1.attendee_id as attendeeId',
                't2.name as attendeeName',

                't1.attendant_id as attendantId',
                't3.name as attendantName',

                $this->database->raw('to_char(start_time, \'HH24:MI\') as "startTime"'),
                $this->database->raw('to_char(end_time, \'HH24:MI\') as "endTime"'),

                'notes',
                'status'
            ]);
    }

    private function formatQueryResult($queryResult)
    {
        $queryResult->notes = stream_get_contents($queryResult->notes);

        return [
            'id' => $queryResult->id,
            'date' => DateTimeImmutable::createFromFormat(
                'Y-m-d',
                $queryResult->date
            ),
            'attendant' => [
                'id' => $queryResult->attendantId,
                'name' => $queryResult->attendantName
            ],
            'pet' => [
                'id' => $queryResult->attendeeId,
                'name' => $queryResult->attendeeName
            ],
            'startTime' => $queryResult->startTime,
            'endTime' => $queryResult->endTime,
            'notes' => $queryResult->notes,
            'status' => $queryResult->status
        ];
    }
}
