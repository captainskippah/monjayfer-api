<?php

namespace App\Scheduler;

use Captainskippah\Scheduler\Domain\AppointmentId;
use Captainskippah\Scheduler\Domain\AppointmentStatus;
use Captainskippah\Scheduler\Domain\Attendant\AttendantId;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeId;
use Captainskippah\Scheduler\Domain\BookingConflictException;
use Captainskippah\Scheduler\Domain\DayRepository;
use Captainskippah\Scheduler\Domain\Schedule;
use Captainskippah\Scheduler\Domain\Time;
use DateTimeImmutable;
use InvalidArgumentException;

class AppointmentService
{
    /**
     * @var DayRepository
     */
    private $dayRepository;

    /**
     * @var AppointmentQueryService
     */
    private $appointmentQueryService;

    public function __construct(
        DayRepository $dayRepository,
        AppointmentQueryService $appointmentQueryService
    ) {
        $this->dayRepository = $dayRepository;
        $this->appointmentQueryService = $appointmentQueryService;
    }

    /**
     * @param DateTimeImmutable $date
     * @param string $petId
     * @param string $attendantId
     * @param string $start
     * @param string $end
     * @param string $notes
     * @return string
     * @throws BookingConflictException
     */
    public function bookAppointment(
        DateTimeImmutable $date,
        string $petId,
        string $attendantId,
        string $start,
        string $end,
        string $notes
    ) {
        $attendeeId = new AttendeeId($petId);
        $attendantId = new AttendantId($attendantId);

        $start = explode(':', $start);
        $end = explode(':', $end);

        $schedule = new Schedule(
            new Time($start[0], $start[1]),
            new Time($end[0], $end[1])
        );

        $day = $this->dayRepository->getDay($date);

        $appointmentId = $day->bookAppointment($attendeeId, $attendantId, $schedule, $notes);

        $this->dayRepository->save($day);

        return $appointmentId->value();
    }

    public function deleteAppointment(string $id)
    {
        $date = $this->findAppointmentDate($id);

        $day = $this->dayRepository->getDay($date);

        $day->deleteAppointment(new AppointmentId($id));

        $this->dayRepository->save($day);
    }

    public function cancelAppointment(string $id)
    {
        $this->changeAppointmentStatus($id, AppointmentStatus::cancelled());
    }

    public function finishAppointment(string $id)
    {
        $this->changeAppointmentStatus($id, AppointmentStatus::done());
    }

    public function scheduleAppointment(string $id)
    {
        $this->changeAppointmentStatus($id, AppointmentStatus::scheduled());
    }

    private function changeAppointmentStatus(string $id, AppointmentStatus $status)
    {
        $date = $this->findAppointmentDate($id);

        $day = $this->dayRepository->getDay($date);

        $day->changeAppointmentStatus(new AppointmentId($id), $status);

        $this->dayRepository->save($day);
    }

    private function findAppointmentDate(string $id)
    {
        $appointment = $this->appointmentQueryService->getById($id);

        if ($appointment === null) {
            throw new InvalidArgumentException('Appointment does not exist');
        }

        return $appointment['date'];
    }
}
