<?php

namespace App\Http\Controllers;

use App\Records\Pet\PetApplicationService;
use App\Records\Pet\PetQueryService;
use DateTimeImmutable;
use Illuminate\Http\Request;

class PetController
{
    /**
     * @var PetApplicationService
     */
    private $petApplicationService;

    /**
     * @var PetQueryService
     */
    private $petQueryService;

    public function __construct(
        PetApplicationService $petApplicationService,
        PetQueryService $petQueryService
    ) {
        $this->petApplicationService = $petApplicationService;
        $this->petQueryService = $petQueryService;
    }

    public function index(string $id)
    {
        return response()->json($this->petQueryService->getByClientId($id));
    }

    public function show(string $id)
    {
        $pet = $this->petQueryService->getById($id);

        if ($pet === null) {
            return response(null, 404);
        }

        return response()->json($pet);
    }

    public function search(Request $request)
    {
        if (!$request->has('query')) {
            return response()->json([]);
        }

        return response()->json($this->petQueryService->search($request->input('query')));
    }

    public function store(string $id, Request $request)
    {
        $response = [
            'clientId' => $id,
            'name' => $request->input('name'),
            'species' => $request->input('species'),
            'breed' => $request->input('breed'),
            'sex' => $request->input('sex'),
            'birthday' => $request->input('birthday')
        ];

        $response['id'] = $this->petApplicationService->create(
            $id,
            $request->input('name'),
            $request->input('species'),
            $request->input('breed'),
            $request->input('sex'),
            DateTimeImmutable::createFromFormat(
                'Y-m-d',
                $request->input('birthday')
            )
        );

        return response()->json($response, 201);
    }

    public function update(string $id, Request $request)
    {
        $this->petApplicationService->update(
            $id,
            $request->input('name'),
            $request->input('species'),
            $request->input('breed'),
            $request->input('sex'),
            DateTimeImmutable::createFromFormat(
                'Y-m-d',
                $request->input('birthday')
            )
        );

        return response(null, 204);
    }

    public function delete(string $id)
    {
        $this->petApplicationService->delete($id);

        return response(null, 204);
    }
}
