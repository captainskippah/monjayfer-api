<?php

namespace App\Http\Controllers;

use App\Auth\PasswordGenerator;
use App\Auth\User;
use App\Records\Client\Client;
use App\Records\Client\ClientId;
use App\Records\Client\ClientRepository;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;

class ClientController
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function index()
    {
        $clients = array_map(function (Client $client) {
            return $this->clientToResponse($client);
        }, $this->clientRepository->getAll());

        return response()->json($clients);
    }

    public function show(string $id)
    {
        $client = $this->clientRepository->getById(new ClientId($id));

        if ($client === null) {
            return response(null, 404);
        }

        return response()->json($this->clientToResponse($client), 201);
    }

    public function store(Request $request)
    {
        $client = new Client(
            $this->clientRepository->nextIdentity(),
            $request->input('name'),
            $request->input('address'),
            $request->input('phone')
        );

        $this->clientRepository->save($client);

        return response()->json($this->clientToResponse($client), 201);
    }

    public function update(string $id, Request $request)
    {
        $client = $this->clientRepository->getById(new ClientId($id));

        if ($client === null) {
            return response(null, 404);
        }

        $client->setName($request->input('name'));
        $client->setAddress($request->input('address'));
        $client->setPhone($request->input('phone'));

        $this->clientRepository->save($client);

        return response(null, 204);
    }

    public function delete(string $id)
    {
        $this->clientRepository->delete(new ClientId($id));

        return response(null, 204);
    }

    public function generateAccount(string $id, Hasher $hasher)
    {
        $client = $this->clientRepository->getById(new ClientId($id));

        if ($client === null) {
            return response(null, 404);
        }

        User::generateCustomerAccount(
            $id,
            $client->name(),
            $client->phone(),
            new PasswordGenerator($hasher)
        );

        return response(null, 204);
    }

    private function clientToResponse(Client $client)
    {
        return [
            'id' => $client->id()->value(),
            'name' => $client->name(),
            'address' => $client->address(),
            'phone' => $client->phone(),
            'account' => User::where('id', $client->id()->value())->exists()
        ];
    }
}
