<?php

namespace App\Http\Controllers;

use App\Inquiry\Inquiry;
use App\Inquiry\InquiryId;
use App\Inquiry\InquiryRepository;
use Illuminate\Http\Request;
use ReCaptcha\ReCaptcha;

class InquiryController extends Controller
{
    /**
     * @var InquiryRepository
     */
    private $inquiryRepository;

    public function __construct(InquiryRepository $inquiryRepository)
    {
        $this->inquiryRepository = $inquiryRepository;
    }

    public function index()
    {
        $inquiries = array_map(function (Inquiry $inquiry) {
            return $this->inquiryToResponse($inquiry);
        }, $this->inquiryRepository->getAll());

        return response()->json($inquiries);
    }

    public function store(Request $request, ReCaptcha $recaptcha)
    {
        $response = $recaptcha->verify(
            $request->input('token'),
            $request->server('REMOTE_ADDR')
        );

        if (!$response->isSuccess()) {
            return response()->json($response->toArray(), 400);
        }

        $inquiry = new Inquiry(
            $this->inquiryRepository->nextIdentity(),
            $request->input('sender'),
            $request->input('message')
        );

        $this->inquiryRepository->save($inquiry);

        return response()->json($this->inquiryToResponse($inquiry), 201);
    }

    public function read(string $id)
    {
        $inquiry = $this->inquiryRepository->getById(new InquiryId($id));

        if ($inquiry === null) {
            return response(null, 404);
        }

        $inquiry->markAsRead();

        $this->inquiryRepository->save($inquiry);

        return response(null, 204);
    }

    public function unread(string $id)
    {
        $inquiry = $this->inquiryRepository->getById(new InquiryId($id));

        if ($inquiry === null) {
            return response(null, 404);
        }

        $inquiry->markAsUnread();

        $this->inquiryRepository->save($inquiry);

        return response(null, 204);
    }

    private function inquiryToResponse(Inquiry $inquiry)
    {
        return [
            'id' => $inquiry->id()->value(),
            'sender' => $inquiry->sender(),
            'message' => $inquiry->message(),
            'isRead' => $inquiry->isRead(),
            'date' => $inquiry->date()->format(DATE_ATOM)
        ];
    }
}
