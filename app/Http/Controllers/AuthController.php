<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;

class AuthController
{
    /**
     * @var StatefulGuard
     */
    private $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function login(Request $request)
    {
        $credentials = [
            'password' => $request->input('password')
        ];

        if ($request->has('username')) {
            $credentials['username'] = $request->input('username');
        } else {
            $credentials['phone'] = $request->input('phone');
        }

        if (!$this->auth->attempt($credentials)) {
            return response('Invalid credentials', 401);
        }
    }

    public function logout(Request $request)
    {
        $this->auth->logout();

        $request->session()->invalidate();
    }
}
