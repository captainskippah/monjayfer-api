<?php

namespace App\Http\Controllers;

use App\Catalog\Product\ProductApplicationService;
use App\Catalog\Product\ProductQueryService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var ProductApplicationService
     */
    private $productApplicationService;

    /**
     * @var ProductQueryService
     */
    private $productQueryService;

    public function __construct(
        ProductApplicationService $productApplicationService,
        ProductQueryService $productQueryService
    ) {
        $this->productApplicationService = $productApplicationService;
        $this->productQueryService = $productQueryService;
    }

    public function index()
    {
        return response()->json($this->productQueryService->getAll());
    }

    public function show(string $id)
    {
        $product = $this->productQueryService->getById($id);

        if ($product === null) {
            return response(null, 404);
        }

        return response()->json($product);
    }

    public function store(Request $request)
    {
        $productId = $this->productApplicationService->newProduct(
            $request->input('name'),
            $request->input('stocks'),
            $request->input('depletingQty'),
            $request->input('price')
        );

        return response()->json([
            'id' => $productId,
            'name' => $request->input('name'),
            'stocks' => $request->input('stocks'),
            'depletingQty' => $request->input('depletingQty'),
            'price' => $request->input('price')
        ], 201);
    }

    public function update(string $id, Request $request)
    {
        $this->productApplicationService->updateProduct(
            $id,
            $request->input('name'),
            $request->input('depletingQty'),
            $request->input('price')
        );

        return response(null, 204);
    }

    public function restock(string $id, Request $request)
    {
        $this->productApplicationService->restockProduct(
            $id,
            $request->input('qty')
        );

        return response(null, 204);
    }

    public function consume(string $id, Request $request)
    {
        $this->productApplicationService->consume(
            $id,
            $request->input('qty')
        );

        return response(null, 204);
    }

    public function delete(string $id)
    {
        $this->productApplicationService->deleteProduct($id);

        return response(null, 204);
    }
}
