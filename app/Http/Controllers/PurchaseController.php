<?php

namespace App\Http\Controllers;

use App\POS\PurchaseTransformer;
use Captainskippah\POS\Domain\Purchase\PurchaseId;
use Captainskippah\POS\Domain\Purchase\PurchaseRepository;

class PurchaseController extends Controller
{
    /**
     * @var PurchaseRepository
     */
    private $purchaseRepository;

    /**
     * @var PurchaseTransformer
     */
    private $purchaseTransformer;

    public function __construct(
        PurchaseRepository $purchaseRepository,
        PurchaseTransformer $purchaseTransformer
    ) {
        $this->purchaseRepository = $purchaseRepository;
        $this->purchaseTransformer = $purchaseTransformer;
    }

    public function index()
    {
        $purchases = $this->purchaseRepository->getAll();

        return response()->json($this->purchaseTransformer->transform(...$purchases));
    }

    public function show(string $id)
    {
        $purchase = $this->purchaseRepository->getById(new PurchaseId($id));

        if ($purchase === null) {
            return response("Purchase with ID '$id' is not found.", 404);
        }

        return response()->json($this->purchaseTransformer->transform($purchase)[0]);
    }
}
