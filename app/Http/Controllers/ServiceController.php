<?php

namespace App\Http\Controllers;

use App\Catalog\Service\ServiceApplicationService;
use App\Catalog\Service\ServiceQueryService;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * @var ServiceApplicationService
     */
    private $serviceApplicationService;

    /**
     * @var ServiceQueryService
     */
    private $serviceQueryService;

    public function __construct(
        ServiceApplicationService $serviceApplicationService,
        ServiceQueryService $serviceQueryService
    ) {
        $this->serviceApplicationService = $serviceApplicationService;
        $this->serviceQueryService = $serviceQueryService;
    }

    public function index()
    {
        return response()->json($this->serviceQueryService->getAll());
    }

    public function show(string $id)
    {
        $item = $this->serviceQueryService->getById($id);

        if ($item === null) {
            return response(null, 404);
        }

        return response()->json($item);
    }

    public function store(Request $request)
    {
        $serviceId = $this->serviceApplicationService->newService(
            $request->input('name'),
            $request->input('price')
        );

        return response()->json([
            'id' => $serviceId,
            'name' => $request->input('name'),
            'price' => $request->input('price')
        ]);
    }

    public function update(string $id, Request $request)
    {
        $this->serviceApplicationService->updateService(
            $id,
            $request->input('name'),
            $request->input('price')
        );

        return response('', 204);
    }

    public function delete(string $id)
    {
        $this->serviceApplicationService->deleteService($id);

        return response(null, 204);
    }
}
