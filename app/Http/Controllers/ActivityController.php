<?php

namespace App\Http\Controllers;

use App\Records\Activity\Activity;
use App\Records\Activity\ActivityId;
use App\Records\Activity\ActivityRepository;
use App\Records\Pet\PetId;
use DateTimeImmutable;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    public function index(string $id)
    {
        $activities = array_map(function (Activity $activity) {
            return $this->activityToResponse($activity);
        }, $this->activityRepository->activitiesOfPet(new PetId($id)));

        return response()->json($activities);
    }

    public function show(string $id)
    {
        $activity = $this->activityRepository->getById(new ActivityId($id));

        if ($activity === null) {
            return response(null, 404);
        }

        return response()->json($this->activityToResponse($activity));
    }

    public function store(string $id, Request $request)
    {
        $activity = new Activity(
            $this->activityRepository->nextIdentity(),
            new PetId($id),
            DateTimeImmutable::createFromFormat(
                'Y-m-d',
                $request->input('date')
            ),
            $request->input('subject'),
            $request->input('activity')
        );

        $this->activityRepository->save($activity);

        return response()->json($this->activityToResponse($activity), 201);
    }

    public function update(string $id, Request $request)
    {
        $activity = $this->activityRepository->getById(new ActivityId($id));

        if ($activity === null) {
            return response(null, 404);
        }

        $activity->setDate(
            DateTimeImmutable::createFromFormat(
                "Y-m-d",
                $request->input('date')
            )
        );

        $activity->setSubject($request->input('subject'));
        $activity->setActivity($request->input('activity'));

        $this->activityRepository->save($activity);

        return response(null, 204);
    }

    public function delete(string $id)
    {
        $this->activityRepository->delete(new ActivityId($id));

        return response(null, 204);
    }

    private function activityToResponse(Activity $activity)
    {
        return [
            'id' => $activity->id()->value(),
            'petId' => $activity->petId()->value(),
            'date' => $activity->date()->format('Y-m-d'),
            'subject' => $activity->subject(),
            'activity' => $activity->activity()
        ];
    }
}
