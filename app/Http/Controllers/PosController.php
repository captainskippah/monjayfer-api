<?php

namespace App\Http\Controllers;

use App\POS\POSApplicationService;
use App\POS\POSSettingsProvider;
use App\POS\PurchaseTransformer;
use Captainskippah\POS\Domain\Payment\PaymentFailedException;
use Illuminate\Http\Request;

class PosController extends Controller
{
    /**
     * @var POSSettingsProvider
     */
    private $settingsProvider;

    /**
     * @var POSApplicationService
     */
    private $posService;

    /**
     * @var PurchaseTransformer
     */
    private $purchaseTransformer;

    public function __construct(
        POSSettingsProvider $settingsProvider,
        POSApplicationService $posService,
        PurchaseTransformer $purchaseTransformer
    ) {
        $this->settingsProvider = $settingsProvider;
        $this->posService = $posService;
        $this->purchaseTransformer = $purchaseTransformer;
    }


    public function settings()
    {
        $settings = [
            'vatRate' => $this->settingsProvider->getVatRate()
        ];

        return response()->json($settings);
    }

    public function checkout(Request $request)
    {
        try {
            $purchase = $this->posService->checkout(
                $request->input('items'),
                $request->input('payment')
            );

            return response()->json(
                $this->purchaseTransformer->transform($purchase)[0]
            );
        } catch (PaymentFailedException $e) {
            return response('Payment failed', 422);
        }
    }
}
