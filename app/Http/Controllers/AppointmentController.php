<?php

namespace App\Http\Controllers;

use App\Scheduler\AppointmentQueryService;
use App\Scheduler\AppointmentService;
use Captainskippah\Scheduler\Domain\BookingConflictException;
use DateTimeImmutable;
use Illuminate\Http\Request;

class AppointmentController
{
    /**
     * @var AppointmentQueryService
     */
    private $appointmentQueryService;

    /**
     * @var AppointmentService
     */
    private $appointmentService;

    public function __construct(
        AppointmentQueryService $dayRepository,
        AppointmentService $appointmentService
    ) {
        $this->appointmentQueryService = $dayRepository;
        $this->appointmentService = $appointmentService;
    }

    public function getByMonth(int $year, int $month)
    {
        $appointments = array_map(function ($appointment) {
            return $this->queryToResponse($appointment);
        }, $this->appointmentQueryService->getByMonth($year, $month));

        return response()->json($appointments);
    }

    public function getById(string $id)
    {
        $appointment = $this->appointmentQueryService->getById($id);

        if ($appointment === null) {
            return response(null, 404);
        }

        return response()->json($this->queryToResponse($appointment));
    }

    public function store(int $year, int $month, int $day, Request $request)
    {
        try {
            $date = sprintf('%s-%02d-%02d', $year, $month, $day);

            $id = $this->appointmentService->bookAppointment(
                DateTimeImmutable::createFromFormat('Y-m-d', $date),
                $request->input('petId'),
                $request->user()->id,
                $request->input('start'),
                $request->input('end'),
                $request->input('notes')
            );

            $response = $this->appointmentQueryService->getById($id);
            $response = $this->queryToResponse($response);

            return response()->json($response, 201);
        } catch (BookingConflictException $e) {
            return response($e->getMessage(), 422);
        }
    }

    public function delete(string $id)
    {
        $this->appointmentService->deleteAppointment($id);

        return response(null, 204);
    }

    public function cancel(string $id)
    {
        $this->appointmentService->cancelAppointment($id);

        return response(null, 204);
    }

    public function done(string $id)
    {
        $this->appointmentService->finishAppointment($id);

        return response(null, 204);
    }

    public function schedule(string $id)
    {
        $this->appointmentService->scheduleAppointment($id);

        return response(null, 204);
    }

    private function queryToResponse(array $appointment)
    {
        $appointment['date'] = $appointment['date']->format("Y-m-d");

        return $appointment;
    }
}
