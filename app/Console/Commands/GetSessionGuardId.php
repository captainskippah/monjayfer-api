<?php

namespace App\Console\Commands;

use Illuminate\Auth\SessionGuard;
use Illuminate\Console\Command;
use Illuminate\Contracts\Auth\Guard;

class GetSessionGuardId extends Command
{
    protected $signature = 'session:id';

    protected $description = 'Get SessionGuard auth id';

    /**
     * @var SessionGuard
     */
    private $guard;

    public function __construct(Guard $guard)
    {
        parent::__construct();

        $this->guard = $guard;
    }

    public function handle()
    {
        if (!$this->guard instanceof SessionGuard) {
            return $this->line('SessionGuard not in use');
        }

        $this->line($this->guard->getName());
    }
}
