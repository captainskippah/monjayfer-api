<?php

namespace App\POS;

use Captainskippah\POS\Domain\Purchase\Purchase;
use Captainskippah\POS\Domain\Purchase\PurchaseItem;

class PurchaseTransformer
{
    public function transform(Purchase ...$purchases): array
    {
        $transformed = [];

        foreach ($purchases as $purchase) {
            $payment['type'] = $purchase->payment()->type();

            if ($purchase->payment()->type() === 'cash') {
                $payment['amount'] = $purchase->payment()->amount();
            }

            $transformed[] = [
                'id' => $purchase->id()->value(),
                'date' => $purchase->transactionDate()->format(DATE_ATOM),

                'items' => array_map(function (PurchaseItem $purchaseItem) {
                    return [
                        'name' => $purchaseItem->name(),
                        'price' => $purchaseItem->price(),
                        'qty' => $purchaseItem->quantity()
                    ];
                }, $purchase->purchaseItems()),

                'payment' => $payment,
                'vatRate' => $purchase->vatRate()
            ];
        }

        return $transformed;
    }
}
