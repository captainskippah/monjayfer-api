<?php

namespace App\POS;

use Captainskippah\POS\Application\CashPayment;
use Captainskippah\POS\Domain\Cart\Cart;
use Captainskippah\POS\Domain\Cart\CartId;
use Captainskippah\POS\Domain\Cart\CheckoutService;
use Captainskippah\POS\Domain\Item\ItemId;
use Captainskippah\POS\Domain\Item\ItemRepository;
use Captainskippah\POS\Domain\Payment\PaymentFailedException;
use Captainskippah\POS\Domain\Purchase\Purchase;
use Captainskippah\POS\Domain\Purchase\PurchaseRepository;
use InvalidArgumentException;

class POSApplicationService
{
    /**
     * @var PurchaseRepository
     */
    private $purchaseRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var POSSettingsProvider
     */
    private $settingsProvider;

    public function __construct(
        PurchaseRepository $purchaseRepository,
        ItemRepository $itemRepository,
        POSSettingsProvider $settingsProvider
    ) {
        $this->purchaseRepository = $purchaseRepository;
        $this->itemRepository = $itemRepository;
        $this->settingsProvider = $settingsProvider;
    }

    /**
     * @param array $items
     * @param array $payment
     * @return Purchase
     * @throws PaymentFailedException
     */
    public function checkout(array $items, array $payment)
    {
        // We use tempId here since we don't store our cart somewhere in our application
        $cart = new Cart(new CartId('tempId'));

        foreach ($items as $item) {
            $cart->addItem(new ItemId($item['id']), $item['qty']);
        }

        if ($payment['type'] !== 'cash') {
            throw new InvalidArgumentException(sprintf("Cash type '%s' not supported.", $payment['type']));
        }

        $checkoutService = new CheckoutService($this->itemRepository);

        $purchase = $checkoutService->checkout(
            $cart,
            new CashPayment($payment['amount']),
            $this->purchaseRepository->nextIdentity(),
            $this->settingsProvider->getVatRate()
        );

        $this->purchaseRepository->save($purchase);

        return $purchase;
    }
}
