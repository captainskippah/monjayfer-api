<?php

namespace App\POS;

class POSSettingsProvider
{
    public function getVatRate(): float
    {
        return 12;
    }
}
