<?php

namespace App\Providers;

use App\Auth\SmsGateway;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Auth\ClicksendSmsGateway;
use ReCaptcha\ReCaptcha;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        /**
         * Inventory
         */
        $this->app->bind(
            \Captainskippah\Inventory\Domain\ProductRepository::class,
            \Infrastructure\Inventory\LumenProductRepository::class
        );

        /**
         * POS
         */
        $this->app->bind(
            \Captainskippah\POS\Domain\Item\ItemRepository::class,
            \Infrastructure\POS\LumenItemRepository::class
        );

        $this->app->bind(
            \Captainskippah\POS\Domain\Purchase\PurchaseRepository::class,
            \Infrastructure\POS\LumenPurchaseRepository::class
        );

        /**
         * Catalog
         */
        $this->app->bind(
            \App\Catalog\Product\ProductRepository::class,
            \Infrastructure\Catalog\LumenProductRepository::class
        );

        $this->app->bind(
            \App\Catalog\Service\ServiceRepository::class,
            \Infrastructure\Catalog\LumenServiceRepository::class
        );

        /**
         * Records
         */
        $this->app->bind(
            \App\Records\Client\ClientRepository::class,
            \Infrastructure\Records\LumenClientRepository::class
        );

        $this->app->bind(
            \App\Records\Pet\PetRepository::class,
            \Infrastructure\Records\LumenPetRepository::class
        );

        $this->app->bind(
            \App\Records\Activity\ActivityRepository::class,
            \Infrastructure\Records\LumenActivityRepository::class
        );

        /**
         * Inquiries
         */
        $this->app->bind(
            \App\Inquiry\InquiryRepository::class,
            \Infrastructure\Inquiry\LumenInquiryRepository::class
        );

        $this->app->bind(ReCaptcha::class, function ($app) {
            return new ReCaptcha(env('RECAPTCHA_SECRET'));
        });

        /**
         * Scheduler
         */
        $this->app->bind(
            \Captainskippah\Scheduler\Domain\DayRepository::class,
            \Infrastructure\Scheduler\LumenDayRepository::class
        );

        $this->app->bind(
            \Captainskippah\Scheduler\Domain\Attendee\AttendeeRepository::class,
            \Infrastructure\Scheduler\LumenAttendeeRepository::class
        );

        /**
         * Auth
         */
        $this->app->bind(SmsGateway::class, function ($app) {
            return new ClicksendSmsGateway(env('CLICKSEND_USERNAME'), env('CLICKSEND_KEY'));
        });
    }
}
