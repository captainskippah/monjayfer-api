<?php

namespace App\Providers;

use App\Auth\CustomerAccountGenerated;
use App\Auth\SendAccountInfo;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        CustomerAccountGenerated::class => [
            SendAccountInfo::class
        ]
    ];
}
