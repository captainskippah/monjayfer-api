<?php

namespace App\Auth;

use Illuminate\Contracts\Hashing\Hasher;

class PasswordGenerator
{
    /**
     * @var Hasher
     */
    private $hasher;

    public function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    public function generatePassword()
    {
        $password = bin2hex(random_bytes(4));

        return $this->hasher->make($password);
    }
}
