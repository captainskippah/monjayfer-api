<?php

namespace App\Auth;

use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use AuthenticatableTrait;

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'username',
        'password',
        'phone',
        'is_employee'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public static function createEmployeeAccount(
        string $id,
        string $name,
        string $username,
        string $password
    ) {
        return static::create([
            'id' => $id,
            'name' => $name,
            'username' => $username,
            'password' => $password,
            'is_employee' => true
        ]);
    }

    public static function generateCustomerAccount(
        string $id,
        string $name,
        string $phone,
        PasswordGenerator $generator
    ) {
        $account = static::create([
            'id' => $id,
            'name' => $name,
            'phone' => $phone,
            'password' => $generator->generatePassword(),
            'is_employee' => false
        ]);

        // Manually dispatch because Eloquent does not
        // fire events when using static method `create`
        event(new CustomerAccountGenerated($account));

        return $account;
    }
}
