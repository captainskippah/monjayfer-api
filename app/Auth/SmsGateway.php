<?php

namespace App\Auth;

interface SmsGateway
{
    public function send(string $phone, string $message);
}
