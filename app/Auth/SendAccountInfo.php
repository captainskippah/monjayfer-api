<?php

namespace App\Auth;

class SendAccountInfo
{
    /**
     * @var SmsGateway
     */
    private $smsGateway;

    public function __construct(SmsGateway $smsGateway)
    {
        $this->smsGateway = $smsGateway;
    }

    public function handle(CustomerAccountGenerated $event)
    {
        $account = $event->user();

        $message = "Hello {$account->name},\n";
        $message .= "Your Monjayfer account is created. ";
        $message .= "You can login using {$account->phone} as your username ";
        $message .= "and {$account->password} as your password.\n";
        $message .= "We advise that you change your password soon.";

        $this->smsGateway->send($account->phone, $message);
    }
}
