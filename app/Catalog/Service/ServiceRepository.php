<?php

namespace App\Catalog\Service;

interface ServiceRepository
{
    /**
     * @return Service[]
     */
    public function getAll(): array;

    public function getById(ServiceId $serviceId): ?Service;

    public function save(Service $service);

    public function delete(ServiceId $serviceId);

    public function nextIdentity(): ServiceId;
}
