<?php

namespace App\Catalog\Service;

use Infrastructure\AbstractLumenRepository;

class ServiceQueryService extends AbstractLumenRepository
{
    public function getAll(): array
    {
        return $this->query()
                ->get()
                ->map(function ($service) {
                    return $this->queryResultToService($service);
                })
                ->all();
    }

    public function getById(string $id): ?array
    {
        $service = $this->query()
            ->where('cs.id', $id)
            ->first();

        if ($service === null) {
            return null;
        }

        return $this->queryResultToService($service);
    }

    private function query()
    {
        return $this->database->table('catalog_services as cs')
                    ->join('pos_items as pi', 'cs.id', '=', 'pi.id')
                    ->select(['cs.id', 'cs.name', 'pi.price']);
    }

    private function queryResultToService($service)
    {
        return [
            'id' => $service->id,
            'name' => $service->name,
            'price' => $service->price
        ];
    }
}
