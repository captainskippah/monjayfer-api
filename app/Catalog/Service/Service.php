<?php

namespace App\Catalog\Service;

class Service
{
    /**
     * @var ServiceId
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    public function __construct(ServiceId $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return ServiceId
     */
    public function id(): ServiceId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    public function rename(string $name)
    {
        $this->name = $name;
    }
}
