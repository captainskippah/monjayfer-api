<?php

namespace App\Catalog\Service;

use App\Catalog\Service\Service;
use App\Catalog\Service\ServiceId;
use App\Catalog\Service\ServiceRepository;
use Captainskippah\POS\Domain\Item\Item;
use Captainskippah\POS\Domain\Item\ItemId;
use Captainskippah\POS\Domain\Item\ItemRepository;

class ServiceApplicationService
{
    /**
     * @var ServiceRepository
     */
    private $serviceCatalogRepository;

    /**
     * @var ItemRepository
     */
    private $posItemRepository;

    public function __construct(
        ServiceRepository $serviceRepository,
        ItemRepository $posItemRepository
    ) {
        $this->serviceCatalogRepository = $serviceRepository;
        $this->posItemRepository = $posItemRepository;
    }

    public function newService(string $name, float $price)
    {
        $catalogService = new Service(
            $this->serviceCatalogRepository->nextIdentity(),
            $name
        );

        $this->serviceCatalogRepository->save($catalogService);

        $item = new Item(
            new ItemId($catalogService->id()->value()),
            $name,
            $price
        );

        $this->posItemRepository->save($item);

        return $catalogService->id()->value();
    }

    public function updateService(string $serviceId, string $name, float $price)
    {
        $catalogService = $this->serviceCatalogRepository->getById(new ServiceId($serviceId));
        $posItem = $this->posItemRepository->getById(new ItemId($serviceId));

        $catalogService->rename($name);
        $posItem->rename($name);

        $posItem->updatePrice($price);

        $this->serviceCatalogRepository->save($catalogService);
        $this->posItemRepository->save($posItem);
    }

    public function deleteService(string $serviceId)
    {
        $this->serviceCatalogRepository->delete(new ServiceId($serviceId));
        $this->posItemRepository->delete(new ItemId($serviceId));
    }
}
