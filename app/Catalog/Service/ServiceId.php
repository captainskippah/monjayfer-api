<?php

namespace App\Catalog\Service;

use Captainskippah\Common\Domain\AbstractId;

class ServiceId extends AbstractId
{
    public function __construct(string $serviceId)
    {
        parent::__construct($serviceId);
    }
}
