<?php

namespace App\Catalog\Product;

use Captainskippah\Common\Domain\AbstractId;

class ProductId extends AbstractId
{
    public function __construct(string $productId)
    {
        parent::__construct($productId);
    }
}
