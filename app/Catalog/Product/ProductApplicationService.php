<?php

namespace App\Catalog\Product;

use App\Catalog\Product\Product as CatalogProduct;
use App\Catalog\Product\ProductId as CatalogProductId;
use App\Catalog\Product\ProductRepository as CatalogProductRepository;
use Captainskippah\Inventory\Domain\Product;
use Captainskippah\Inventory\Domain\ProductId as InventoryProductId;
use Captainskippah\Inventory\Domain\ProductId;
use Captainskippah\Inventory\Domain\ProductRepository as InventoryProductRepository;
use Captainskippah\POS\Domain\Item\Item;
use Captainskippah\POS\Domain\Item\ItemId;
use Captainskippah\POS\Domain\Item\ItemRepository;

class ProductApplicationService
{
    /**
     * @var InventoryProductRepository
     */
    private $inventoryProductRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var CatalogProductRepository
     */
    private $catalogProductRepository;

    public function __construct(
        InventoryProductRepository $inventoryProductRepository,
        ItemRepository $itemRepository,
        CatalogProductRepository $catalogProductRepository
    ) {
        $this->inventoryProductRepository = $inventoryProductRepository;
        $this->itemRepository = $itemRepository;
        $this->catalogProductRepository = $catalogProductRepository;
    }

    public function newProduct(
        string $name,
        int $initialQty,
        int $depletingQty,
        float $price
    ) {
        $catalogProduct = new CatalogProduct(
            $this->catalogProductRepository->nextIdentity(),
            $name
        );

        $this->catalogProductRepository->save($catalogProduct);

        $productId = $catalogProduct->id()->value();

        $this->inventoryProductRepository->save(
            new Product(
                new ProductId($productId),
                $name,
                $initialQty,
                $depletingQty
            )
        );

        $this->itemRepository->save(
            new Item(
                new ItemId($productId),
                $name,
                $price
            )
        );

        return $productId;
    }

    public function updateProduct(
        string $productId,
        string $name,
        int $depletingQty,
        float $price
    ) {
        $product = $this->inventoryProductRepository->getById(new InventoryProductId($productId));
        $posItem = $this->itemRepository->getById(new ItemId($productId));
        $catalogEntry = $this->catalogProductRepository->getById(new CatalogProductId($productId));

        $product->rename($name);
        $posItem->rename($name);
        $catalogEntry->rename($name);

        $product->updateDepletingQty($depletingQty);
        $posItem->updatePrice($price);

        $this->inventoryProductRepository->save($product);
        $this->itemRepository->save($posItem);
        $this->catalogProductRepository->save($catalogEntry);
    }

    public function deleteProduct(string $productId)
    {
        $this->inventoryProductRepository->delete(new InventoryProductId($productId));
        $this->itemRepository->delete(new ItemId($productId));
        $this->catalogProductRepository->delete(new CatalogProductId($productId));
    }

    public function restockProduct(string $id, int $qty)
    {
        $product = $this->inventoryProductRepository->getById(new ProductId($id));

        $product->restock($qty);

        $this->inventoryProductRepository->save($product);
    }

    public function consume(string $id, int $qty)
    {
        $product = $this->inventoryProductRepository->getById(new ProductId($id));

        $product->take($qty);

        $this->inventoryProductRepository->save($product);
    }
}
