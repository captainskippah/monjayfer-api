<?php

namespace App\Catalog\Product;

use Infrastructure\AbstractLumenRepository;

class ProductQueryService extends AbstractLumenRepository
{
    public function getAll(): array
    {
        return $this->query()
                ->get()
                ->map(function ($product) {
                    return $this->queryResultToProduct($product);
                })
                ->all();
    }

    public function getById(string $id): ?array
    {
        $product = $this->query()
            ->where('cp.id', $id)
            ->first();

        if ($product === null) {
            return null;
        }

        return $this->queryResultToProduct($product);
    }

    private function query()
    {
        return $this->database
            ->table('catalog_products as cp')
            ->join('inventory_products as ip', 'cp.id', '=', 'ip.id')
            ->join('pos_items as pi', 'cp.id', '=', 'pi.id')
            ->select([
                'cp.id',
                'cp.name',

                'ip.qty',
                'ip.depleting_qty',

                'pi.price'
            ]);
    }

    private function queryResultToProduct($result)
    {
        return [
            'id' => $result->id,
            'name' => $result->name,
            'stocks' => $result->qty,
            'depletingQty' => $result->depleting_qty,
            'price' => $result->price
        ];
    }
}
