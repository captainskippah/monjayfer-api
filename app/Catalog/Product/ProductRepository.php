<?php

namespace App\Catalog\Product;

interface ProductRepository
{
    /**
     * @return Product[]
     */
    public function getAll(): array;

    public function getById(ProductId $productId): ?Product;

    public function save(Product $product);

    public function delete(ProductId $productId);

    public function nextIdentity(): ProductId;
}
