<?php

namespace App\Inquiry;

use DateTimeImmutable;

class Inquiry
{
    /**
     * @var InquiryId
     */
    private $id;

    /**
     * @var string
     */
    private $sender;

    /**
     * @var string
     */
    private $message;

    /**
     * @var DateTimeImmutable
     */
    private $date;

    /**
     * @var bool
     */
    private $isRead = false;

    public function __construct(InquiryId $id, string $sender, string $message)
    {
        $this->id = $id;
        $this->sender = $sender;
        $this->message = $message;
        $this->date = date_create_immutable();
    }

    public function id(): InquiryId
    {
        return $this->id;
    }

    public function message(): string
    {
        return $this->message;
    }

    public function sender(): string
    {
        return $this->sender;
    }

    public function isRead(): bool
    {
        return $this->isRead;
    }

    public function markAsRead()
    {
        $this->isRead = true;
    }

    public function markAsUnread()
    {
        $this->isRead = false;
    }

    public function date(): DateTimeImmutable
    {
        return $this->date;
    }
}
