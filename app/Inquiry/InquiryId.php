<?php

namespace App\Inquiry;

use Captainskippah\Common\Domain\AbstractId;

class InquiryId extends AbstractId
{
    public function __construct(string $inquiryId)
    {
        parent::__construct($inquiryId);
    }
}
