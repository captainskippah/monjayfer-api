<?php

namespace App\Inquiry;

interface InquiryRepository
{
    /**
     * @return Inquiry[]
     */
    public function getAll(): array;

    public function getById(InquiryId $inquiryId): ?Inquiry;

    public function save(Inquiry $inquiry);

    public function nextIdentity(): InquiryId;
}
