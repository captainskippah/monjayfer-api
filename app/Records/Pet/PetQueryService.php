<?php

namespace App\Records\Pet;

use Infrastructure\AbstractLumenRepository;

class PetQueryService extends AbstractLumenRepository
{
    public function getById(string $petId): ?array
    {
        $pet = $this->query()->where('pet.id', $petId)->first();

        if ($pet === null) {
            return null;
        }

        return $this->formatQueryResult($pet);
    }

    public function getByClientId(string $clientId): array
    {
        return $this->query()
            ->where('client_id', $clientId)
            ->get()
            ->transform(function ($queryResult) {
                return $this->formatQueryResult($queryResult);
            })
            ->all();
    }

    public function search(string $query): array
    {
        if (empty($query)) {
            return [];
        }

        return $this->query()
            ->where('pet.name', 'ILIKE', "%{$query}%")
            ->get()
            ->transform(function ($queryResult) {
                return $this->formatQueryResult($queryResult);
            })
            ->all();
    }

    private function query()
    {
        return $this->database->table('record_pets as pet')
            ->join('record_clients as client', 'pet.client_id', '=', 'client.id')
            ->select([
                'pet.id',
                'pet.name',
                'pet.species',
                'pet.breed',
                'pet.sex',
                'pet.birthday',

                'client.id as clientId',
                'client.name as clientName'
            ]);
    }

    private function formatQueryResult($queryResult)
    {
        return [
            'id' => $queryResult->id,
            'name' => $queryResult->name,
            'species' => $queryResult->species,
            'breed' => $queryResult->breed,
            'sex' => $queryResult->sex,
            'birthday' => $queryResult->birthday,
            'client' => [
                'id' => $queryResult->clientId,
                'name' => $queryResult->clientName
            ]
        ];
    }
}
