<?php

namespace App\Records\Pet;

use App\Records\Client\ClientId;

interface PetRepository
{
    /**
     * @param ClientId $clientId
     * @return Pet[]
     */
    public function allPetsOfClient(ClientId $clientId): array;

    public function getById(PetId $petId): ?Pet;

    public function save(Pet $pet);

    public function delete(PetId $petId);

    public function nextIdentity(): PetId;
}
