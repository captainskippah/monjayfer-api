<?php

namespace App\Records\Pet;

use Captainskippah\Common\Domain\AbstractId;

class PetId extends AbstractId
{
    public function __construct(string $petId)
    {
        parent::__construct($petId);
    }
}
