<?php

namespace App\Records\Pet;

use InvalidArgumentException;

class Sex
{
    public const MALE = 1;

    public const FEMALE = 2;

    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        if ($value !== static::MALE && $value !== static::FEMALE) {
            throw new InvalidArgumentException("Invalid sex code");
        }

        $this->value = $value;
    }

    public function value()
    {
        return $this->value;
    }

    public function equal(self $other)
    {
        return $this->value() === $other->value();
    }

    public static function male()
    {
        return new self(1);
    }

    public static function female()
    {
        return new self(2);
    }
}
