<?php

namespace App\Records\Pet;

use App\Records\Client\ClientId;
use DateTimeImmutable;

class Pet
{
    /**
     * @var PetId
     */
    private $id;

    /**
     * @var ClientId
     */
    private $clientId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $species;

    /**
     * @var string
     */
    private $breed;

    /**
     * @var Sex
     */
    private $sex;

    /**
     * @var DateTimeImmutable
     */
    private $birthday;

    public function __construct(
        PetId $id,
        ClientId $clientId,
        string $name,
        string $species,
        string $breed,
        Sex $sex,
        DateTimeImmutable $birthday
    ) {
        $this->id = $id;
        $this->clientId = $clientId;
        $this->name = $name;
        $this->species = $species;
        $this->breed = $breed;
        $this->sex = $sex;
        $this->setBirthday($birthday);
    }

    public function id(): PetId
    {
        return $this->id;
    }

    public function clientId(): ClientId
    {
        return $this->clientId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function species(): string
    {
        return $this->species;
    }

    public function setSpecies(string $species): void
    {
        $this->species = $species;
    }

    public function breed(): string
    {
        return $this->breed;
    }

    public function setBreed(string $breed): void
    {
        $this->breed = $breed;
    }

    public function sex(): Sex
    {
        return $this->sex;
    }

    public function setSex(Sex $sex): void
    {
        $this->sex = $sex;
    }

    public function birthday(): DateTimeImmutable
    {
        return $this->birthday;
    }

    public function setBirthday(DateTimeImmutable $birthday): void
    {
        $this->birthday = DateTimeImmutable::createFromFormat('Y-m-d', $birthday->format('Y-m-d'));
    }
}
