<?php

namespace App\Records\Pet;

use App\Records\Client\ClientId;
use Captainskippah\Scheduler\Domain\Attendee\Attendee;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeId;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeRepository;
use DateTimeImmutable;

class PetApplicationService
{
    /**
     * @var PetRepository
     */
    private $petRepository;

    /**
     * @var AttendeeRepository
     */
    private $attendeeRepository;

    public function __construct(
        PetRepository $petRepository,
        AttendeeRepository $attendeeRepository
    ) {
        $this->petRepository = $petRepository;
        $this->attendeeRepository = $attendeeRepository;
    }

    public function create(
        string $clientId,
        string $name,
        string $species,
        string $breed,
        int $sex,
        DateTimeImmutable $birthday
    ) {
        $pet = new Pet(
            $this->petRepository->nextIdentity(),
            new ClientId($clientId),
            $name,
            $species,
            $breed,
            new Sex($sex),
            $birthday
        );

        $this->petRepository->save($pet);

        $attendee = new Attendee(
            new AttendeeId($pet->id()->value()),
            $pet->name()
        );

        $this->attendeeRepository->save($attendee);

        return $pet->id()->value();
    }

    public function update(
        string $petId,
        string $name,
        string $species,
        string $breed,
        int $sex,
        DateTimeImmutable $birthday
    ) {
        $pet = $this->petRepository->getById(new PetId($petId));

        $pet->setName($name);
        $pet->setSpecies($species);
        $pet->setBreed($breed);
        $pet->setSex(new Sex($sex));
        $pet->setBirthday($birthday);

        $this->petRepository->save($pet);

        $attendee = $this->attendeeRepository->getById(new AttendeeId($petId));
        $attendee->setName($name);

        $this->attendeeRepository->save($attendee);
    }

    public function delete(string $id)
    {
        $this->attendeeRepository->delete(new AttendeeId($id));
        $this->petRepository->delete(new PetId($id));
    }
}
