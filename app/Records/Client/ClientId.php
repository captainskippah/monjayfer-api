<?php

namespace App\Records\Client;

use Captainskippah\Common\Domain\AbstractId;

class ClientId extends AbstractId
{
    public function __construct(string $clientId)
    {
        parent::__construct($clientId);
    }
}
