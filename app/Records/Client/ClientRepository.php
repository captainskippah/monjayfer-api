<?php

namespace App\Records\Client;

interface ClientRepository
{
    /**
     * @return Client[]
     */
    public function getAll(): array;

    public function getById(ClientId $clientId): ?Client;

    public function save(Client $client);

    public function delete(ClientId $clientId);

    public function nextIdentity(): ClientId;
}
