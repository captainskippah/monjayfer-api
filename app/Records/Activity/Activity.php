<?php

namespace App\Records\Activity;

use App\Records\Pet\PetId;
use DateTimeImmutable;

class Activity
{
    /**
     * @var ActivityId
     */
    private $id;

    /**
     * @var PetId
     */
    private $petId;

    /**
     * @var DateTimeImmutable
     */
    private $date;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $activity;

    public function __construct(
        ActivityId $id,
        PetId $petId,
        DateTimeImmutable $date,
        string $subject,
        string $activity
    ) {
        $this->id = $id;
        $this->petId = $petId;
        $this->date = $date;
        $this->subject = $subject;
        $this->activity = $activity;
    }

    public function id(): ActivityId
    {
        return $this->id;
    }

    public function petId(): PetId
    {
        return $this->petId;
    }

    public function date(): DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(DateTimeImmutable $date): void
    {
        $this->date = $date;
    }

    public function subject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function activity(): string
    {
        return $this->activity;
    }

    public function setActivity(string $activity): void
    {
        $this->activity = $activity;
    }
}
