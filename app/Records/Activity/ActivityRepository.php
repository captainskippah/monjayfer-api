<?php

namespace App\Records\Activity;

use App\Records\Pet\PetId;

interface ActivityRepository
{
    /**
     * @param PetId $petId
     * @return Activity[]
     */
    public function activitiesOfPet(PetId $petId): array;

    public function getById(ActivityId $activityId): ?Activity;

    public function save(Activity $activity);

    public function delete(ActivityId $activityId);

    public function nextIdentity(): ActivityId;
}
