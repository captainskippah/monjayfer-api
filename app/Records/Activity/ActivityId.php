<?php

namespace App\Records\Activity;

use Captainskippah\Common\Domain\AbstractId;

class ActivityId extends AbstractId
{
    public function __construct(string $activityId)
    {
        parent::__construct($activityId);
    }
}
