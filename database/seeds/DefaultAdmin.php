<?php

use App\Auth\User;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class DefaultAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hasher = $this->container->make('hash');

        User::createEmployeeAccount(
            Uuid::uuid4()->serialize(),
            'Admin',
            'admin',
            $hasher->make('password')
        );
    }
}
