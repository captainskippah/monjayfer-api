<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler_appointments', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('attendee_id');
            $table->uuid('attendant_id');
            $table->date('date');
            $table->time('start_time');
            $table->time('end_time');
            $table->binary('notes');
            $table->string('status');

            $table->primary('id');

            $table->foreign('attendee_id')
                ->references('id')->on('scheduler_attendees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduler_appointments');
    }
}
