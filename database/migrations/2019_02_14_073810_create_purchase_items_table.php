<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_purchase_items', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('purchase_id');
            $table->string('name');
            $table->decimal('price', 12, 2);
            $table->integer('qty');

            $table->foreign('purchase_id')
                ->references('id')
                ->on('pos_purchases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_purchase_items');
    }
}
