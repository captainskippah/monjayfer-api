<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordPetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_pets', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('client_id');
            $table->string('name');
            $table->string('species');
            $table->string('breed');
            $table->tinyInteger('sex');
            $table->date('birthday');

            $table->primary('id');

            $table->foreign('client_id')
                ->references('id')->on('record_clients')
                ->onDelete('cascade');
        });

        $db = app('db');
        $db->statement('CREATE EXTENSION IF NOT EXISTS pg_trgm');
        $db->statement('CREATE INDEX record_pets_gin ON record_pets USING gin(name gin_trgm_ops)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        app('db')->statement("DROP INDEX IF EXISTS record_pets_gin");
        Schema::dropIfExists('record_pets');
    }
}
