<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_purchase_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('purchase_id');
            $table->decimal('amount', 12, 2);

            $table->foreign('purchase_id')
                ->references('id')
                ->on('pos_purchases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_purchase_payments');
    }
}
