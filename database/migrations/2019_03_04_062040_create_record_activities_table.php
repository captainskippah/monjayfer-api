<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_activities', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('pet_id');
            $table->string('subject');
            $table->binary('activity');
            $table->dateTime('date_created');

            $table->primary('id');

            $table->foreign('pet_id')
                ->references('id')->on('record_pets')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_activities');
    }
}
