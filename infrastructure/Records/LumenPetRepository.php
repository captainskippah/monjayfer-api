<?php

namespace Infrastructure\Records;

use App\Records\Client\ClientId;
use App\Records\Pet\Pet;
use App\Records\Pet\PetId;
use App\Records\Pet\PetRepository;
use App\Records\Pet\Sex;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenPetRepository extends AbstractLumenRepository implements PetRepository
{
    public function allPetsOfClient(ClientId $clientId): array
    {
        return $this->query()
                ->where('client_id', $clientId->value())
                ->get()
                ->map(function ($query) {
                    return $this->queryResultToModel($query);
                })
                ->all();
    }

    public function getById(PetId $petId): ?Pet
    {
        $pet = $this->query()
            ->where('id', $petId->value())
            ->first();

        if ($pet === null) {
            return null;
        }

        return $this->queryResultToModel($pet);
    }

    public function save(Pet $pet)
    {
        if ($this->query()->where('id', $pet->id()->value())->exists()) {
            $this->query()
                ->where('id', $pet->id()->value())
                ->update([
                    'name' => $pet->name(),
                    'species' => $pet->species(),
                    'breed' => $pet->breed(),
                    'sex' => $pet->sex()->value(),
                    'birthday' => $pet->birthday()->format('Y-m-d')
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $pet->id()->value(),
                    'client_id' => $pet->clientId()->value(),
                    'name' => $pet->name(),
                    'species' => $pet->species(),
                    'breed' => $pet->breed(),
                    'sex' => $pet->sex()->value(),
                    'birthday' => $pet->birthday()->format('Y-m-d')
                ]);
        }
    }

    public function delete(PetId $petId)
    {
        $this->query()->delete($petId->value());
    }

    public function nextIdentity(): PetId
    {
        return new PetId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table('record_pets');
    }

    private function queryResultToModel($query)
    {
        return new Pet(
            new PetId($query->id),
            new ClientId($query->client_id),
            $query->name,
            $query->species,
            $query->breed,
            new Sex($query->sex),
            date_create_immutable($query->birthday)
        );
    }
}
