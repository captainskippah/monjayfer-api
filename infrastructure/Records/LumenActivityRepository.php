<?php

namespace Infrastructure\Records;

use App\Records\Activity\Activity;
use App\Records\Activity\ActivityId;
use App\Records\Activity\ActivityRepository;
use App\Records\Pet\PetId;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenActivityRepository extends AbstractLumenRepository implements ActivityRepository
{
    public function activitiesOfPet(PetId $petId): array
    {
        return $this->query()
            ->where('pet_id', $petId->value())
            ->get()
            ->map(function ($inquiry) {
                return $this->queryResultToModel($inquiry);
            })
            ->all();
    }

    public function getById(ActivityId $activityId): ?Activity
    {
        $activity = $this->query()
            ->where('id', $activityId->value())
            ->first();

        if ($activity === null) {
            return null;
        }

        return $this->queryResultToModel($activity);
    }

    public function save(Activity $activity)
    {
        if ($this->query()->where('id', $activity->id()->value())->exists()) {
            $this->query()
                ->where('id', $activity->id()->value())
                ->update([
                    'date_created' => $activity->date()->format('Y-m-d H:i:sO'),
                    'subject' => $activity->subject(),
                    'activity' => $activity->activity()
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $activity->id()->value(),
                    'pet_id' => $activity->petId()->value(),
                    'date_created' => $activity->date()->format('Y-m-d H:i:sO'),
                    'subject' => $activity->subject(),
                    'activity' => $activity->activity()
                ]);
        }
    }

    public function delete(ActivityId $activityId)
    {
        $this->query()->delete($activityId->value());
    }

    public function nextIdentity(): ActivityId
    {
        return new ActivityId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table('record_activities');
    }

    private function queryResultToModel($query)
    {
        return new Activity(
            new ActivityId($query->id),
            new PetId($query->pet_id),
            date_create_immutable($query->date_created),
            $query->subject,
            stream_get_contents($query->activity)
        );
    }
}
