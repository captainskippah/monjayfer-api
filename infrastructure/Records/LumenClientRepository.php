<?php

namespace Infrastructure\Records;

use App\Records\Client\Client;
use App\Records\Client\ClientId;
use App\Records\Client\ClientRepository;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenClientRepository extends AbstractLumenRepository implements ClientRepository
{
    /**
     * @return Client[]
     */
    public function getAll(): array
    {
        return $this->query()
            ->get()
            ->map(function ($query) {
                return $this->queryResultToModel($query);
            })
            ->all();
    }

    public function getById(ClientId $clientId): ?Client
    {
        $client = $this->query()
            ->where('id', $clientId->value())
            ->first();

        if ($client === null) {
            return null;
        }

        return $this->queryResultToModel($client);
    }

    public function save(Client $client)
    {
        if ($this->query()->where('id', $client->id()->value())->exists()) {
            $this->query()
                ->where('id', $client->id()->value())
                ->update([
                    'name' => $client->name(),
                    'address' => $client->address(),
                    'phone' => $client->phone()
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $client->id()->value(),
                    'name' => $client->name(),
                    'address' => $client->address(),
                    'phone' => $client->phone()
                ]);
        }
    }

    public function delete(ClientId $clientId)
    {
        $this->query()->delete($clientId->value());
    }

    public function nextIdentity(): ClientId
    {
        return new ClientId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table('record_clients');
    }

    private function queryResultToModel($query)
    {
        return new Client(
            new ClientId($query->id),
            $query->name,
            $query->address,
            $query->phone
        );
    }
}
