<?php

namespace Infrastructure\Catalog;

use App\Catalog\Product\Product;
use App\Catalog\Product\ProductId;
use App\Catalog\Product\ProductRepository;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenProductRepository extends AbstractLumenRepository implements ProductRepository
{
    public const TABLE_NAME = 'catalog_products';

    /**
     * @return Product[]
     */
    public function getAll(): array
    {
        $products = $this->query()->get();

        return $products
                ->map(function ($product) {
                    return $this->queryResultToModel($product);
                })
                ->all();
    }

    public function getById(ProductId $productId): ?Product
    {
        $product = $this->query()->where('id', $productId->value())->first();

        if ($product === null) {
            return null;
        }

        return $this->queryResultToModel($product);
    }

    public function save(Product $product)
    {
        if ($this->query()->where('id', $product->id()->value())->exists()) {
            $this->query()
                ->where('id', $product->id()->value())
                ->update([
                    'name' => $product->name()
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $product->id()->value(),
                    'name' => $product->name()
                ]);
        }
    }

    public function delete(ProductId $productId)
    {
        $this->query()->delete($productId->value());
    }

    public function nextIdentity(): ProductId
    {
        return new ProductId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    private function queryResultToModel($product)
    {
        return new Product(new ProductId($product->id), $product->name);
    }
}
