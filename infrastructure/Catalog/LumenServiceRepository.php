<?php

namespace Infrastructure\Catalog;

use App\Catalog\Service\Service;
use App\Catalog\Service\ServiceId;
use App\Catalog\Service\ServiceRepository;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenServiceRepository extends AbstractLumenRepository implements ServiceRepository
{
    public const TABLE_NAME = 'catalog_services';

    public function getAll(): array
    {
        return $this->query()
            ->get()
            ->map(function ($service) {
                return $this->queryResultToModel($service);
            })
            ->all();
    }

    public function getById(ServiceId $serviceId): ?Service
    {
        $service = $this->query()->where('id', $serviceId->value())->first();

        if ($service === null) {
            return null;
        }

        return $this->queryResultToModel($service);
    }

    public function save(Service $service)
    {
        if ($this->query()->where('id', $service->id()->value())->exists()) {
            $this->query()
                ->where('id', $service->id()->value())
                ->update([
                    'name' => $service->name()
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $service->id()->value(),
                    'name' => $service->name()
                ]);
        }
    }

    public function delete(ServiceId $serviceId)
    {
        $this->query()->delete($serviceId->value());
    }

    public function nextIdentity(): ServiceId
    {
        return new ServiceId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    private function queryResultToModel($service)
    {
        return new Service(new ServiceId($service->id), $service->name);
    }
}
