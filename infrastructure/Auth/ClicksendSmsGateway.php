<?php

namespace Infrastructure\Auth;

use App\Auth\SmsGateway;

class ClicksendSmsGateway implements SmsGateway
{
    private const BASE_URL = 'https://rest.clicksend.com/v3';

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $key;

    public function __construct(string $username, string $key)
    {
        $this->username = $username;
        $this->key = $key;
    }

    public function send(string $phone, string $message)
    {
        $ch = curl_init(self::BASE_URL . '/sms/send');

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode(sprintf('%s:%s', $this->username, $this->key))
        ]);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            'messages' => [
                [
                    'to' => $phone,
                    'body' => $message,
                    'source' => 'lumen'
                ]
            ]
        ]));

        curl_exec($ch);
        curl_close($ch);
    }
}
