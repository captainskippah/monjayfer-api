<?php

namespace Infrastructure;

use Captainskippah\Common\Domain\AbstractId;
use Illuminate\Database\Connection;

abstract class AbstractLumenRepository
{
    /**
     * @var Connection
     */
    protected $database;

    public function __construct()
    {
        $this->database = app('db')->connection();
    }
}
