<?php

namespace Infrastructure\Inventory;

use Captainskippah\Inventory\Domain\Product;
use Captainskippah\Inventory\Domain\ProductId;
use Captainskippah\Inventory\Domain\ProductRepository;
use Illuminate\Support\Str;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenProductRepository extends AbstractLumenRepository implements ProductRepository
{
    public const TABLE_NAME = 'inventory_products';

    public function getById(ProductId $productId): ?Product
    {
        $query = $this->query()->where(['id' => $productId->value()])->first();

        if ($query === null) {
            return null;
        }

        return $this->queryToModel($query);
    }

    public function save(Product $product)
    {
        if ($this->query()->where('id', $product->id()->value())->exists()) {
            $this->query()
                ->where('id', $product->id()->value())
                ->update([
                    'name' => $product->name(),
                    'qty' => $product->stockQty(),
                    'depleting_qty' => $product->depletingQty()
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $product->id()->value(),
                    'name' => $product->name(),
                    'qty' => $product->stockQty(),
                    'depleting_qty' => $product->depletingQty()
                ]);
        }
    }

    public function delete(ProductId $productId)
    {
        $this->query()->delete($productId->value());
    }

    public function nextIdentity(): ProductId
    {
        return new ProductId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    private function queryToModel($queryResult)
    {
        $product = new Product(
            new ProductId($queryResult->id),
            $queryResult->name,
            $queryResult->qty,
            $queryResult->depleting_qty
        );

        return $product;
    }
}
