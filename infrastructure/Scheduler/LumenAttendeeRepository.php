<?php

namespace Infrastructure\Scheduler;

use Captainskippah\Scheduler\Domain\Attendee\Attendee;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeId;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeRepository;
use Infrastructure\AbstractLumenRepository;

class LumenAttendeeRepository extends AbstractLumenRepository implements AttendeeRepository
{
    public function getById(AttendeeId $attendeeId): ?Attendee
    {
        $attendee = $this->query()->find($attendeeId->value());

        if ($attendee === null) {
            return null;
        }

        return $this->queryResultToModel($attendee);
    }

    public function save(Attendee $attendee)
    {
        if ($this->query()->where('id', $attendee->id()->value())->exists()) {
            $this->query()
                ->where('id', $attendee->id()->value())
                ->update([
                    'name' => $attendee->name()
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $attendee->id()->value(),
                    'name' => $attendee->name()
                ]);
        }
    }

    public function delete(AttendeeId $attendeeId)
    {
        $this->query()->delete($attendeeId->value());
    }

    private function query()
    {
        return $this->database->table('scheduler_attendees');
    }

    private function queryResultToModel($queryResult)
    {
        return new Attendee(new AttendeeId($queryResult->id), $queryResult->name);
    }
}
