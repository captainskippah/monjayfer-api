<?php

namespace Infrastructure\Scheduler;

use Captainskippah\Scheduler\Domain\Appointment;
use Captainskippah\Scheduler\Domain\AppointmentId;
use Captainskippah\Scheduler\Domain\AppointmentStatus;
use Captainskippah\Scheduler\Domain\Attendant\AttendantId;
use Captainskippah\Scheduler\Domain\Attendee\AttendeeId;
use Captainskippah\Scheduler\Domain\Day;
use Captainskippah\Scheduler\Domain\DayRepository;
use Captainskippah\Scheduler\Domain\Schedule;
use Captainskippah\Scheduler\Domain\Time;
use DateTimeImmutable;
use Infrastructure\AbstractLumenRepository;

class LumenDayRepository extends AbstractLumenRepository implements DayRepository
{
    public function getDay(DateTimeImmutable $date): Day
    {
        $day = new Day($date);

        $appointments = $this->database
            ->table('scheduler_appointments')
            ->select([
                'id',
                'attendee_id',
                'attendant_id',

                $this->database->raw('EXTRACT(HOUR FROM start_time) as start_hour'),
                $this->database->raw('EXTRACT(MINUTE FROM start_time) as start_minute'),

                $this->database->raw('EXTRACT(HOUR FROM end_time) as end_hour'),
                $this->database->raw('EXTRACT(MINUTE FROM end_time) as end_minute'),

                'notes',
                'status'
            ])
            ->where('date', $date->format('Y-m-d'))
            ->get()
            ->map(function ($queryResult) use ($date) {
                return new Appointment(
                    new AppointmentId($queryResult->id),
                    $date,
                    new AttendeeId($queryResult->attendee_id),
                    new AttendantId($queryResult->attendant_id),
                    new Schedule(
                        new Time($queryResult->start_hour, $queryResult->start_minute),
                        new Time($queryResult->end_hour, $queryResult->end_minute)
                    ),
                    stream_get_contents($queryResult->notes),
                    new AppointmentStatus($queryResult->status)
                );
            })
            ->all();

        $reflection = new \ReflectionProperty(Day::class, 'appointments');
        $reflection->setAccessible(true);
        $reflection->setValue($day, $appointments);

        return $day;
    }

    public function save(Day $day)
    {
        $appointments = collect($day->appointments())->map(function (Appointment $appointment) {
            return [
                'id' => $appointment->id()->value(),
                'attendee_id' => $appointment->attendeeId()->value(),
                'attendant_id' => $appointment->attendantId()->value(),
                'date' => $appointment->date()->format('Y-m-d'),
                'start_time' => $this->formatTimeToPostgres($appointment->schedule()->startTime()),
                'end_time' => $this->formatTimeToPostgres($appointment->schedule()->endTime()),
                'notes' => $appointment->notes(),
                'status' => $appointment->status()->value()
            ];
        });

        // Delete previous appointments that are not part of Day anymore
        $this->query()
            ->whereDate('date', $day->date())
            ->whereNotIn('id', $appointments->pluck('id'))
            ->delete();

        // Update existing appointments
        $existingIds = $this->query()
            ->whereDate('date', $day->date())
            ->pluck('id')
            ->all();

        $appointments->whereIn('id', $existingIds)
            ->each(function ($appointment, $index) {
                $this->query()
                    ->where('id', array_pull($appointment, 'id'))
                    ->update($appointment);
            });

        // Insert new appointments
        $values = $appointments->whereNotIn('id', $existingIds)->all();

        $this->query()->insert($values);
    }

    private function query()
    {
        return $this->database->table('scheduler_appointments');
    }

    private function formatTimeToPostgres(Time $time)
    {
        return sprintf('%02d:%02d', $time->hour(), $time->minutes());
    }
}
