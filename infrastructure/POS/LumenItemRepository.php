<?php

namespace Infrastructure\POS;

use Captainskippah\POS\Domain\Item\Item;
use Captainskippah\POS\Domain\Item\ItemId;
use Captainskippah\POS\Domain\Item\ItemRepository;
use Illuminate\Support\Str;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenItemRepository extends AbstractLumenRepository implements ItemRepository
{
    public const TABLE_NAME = 'pos_items';

    public function getById(ItemId $itemId): ?Item
    {
        $query = $this->query()->where(['id' => $itemId->value()])->first();

        if ($query === null) {
            return null;
        }

        return $this->queryResultToModel($query);
    }

    public function save(Item $item)
    {
        if ($this->query()->where('id', $item->id()->value())->exists()) {
            $this->query()
                ->where('id', $item->id()->value())
                ->update([
                    'name' => $item->name(),
                    'price' => $item->price()
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $item->id()->value(),
                    'name' => $item->name(),
                    'price' => $item->price()
                ]);
        }
    }

    public function delete(ItemId $itemId)
    {
        $this->query()->delete($itemId->value());
    }

    public function nextIdentity(): ItemId
    {
        return new ItemId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    private function queryResultToModel($queryResult)
    {
        return new Item(
            new ItemId($queryResult->id),
            $queryResult->name,
            $queryResult->price
        );
    }
}
