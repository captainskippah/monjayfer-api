<?php

namespace Infrastructure\POS;

use Captainskippah\POS\Application\CashPayment;
use Captainskippah\POS\Domain\Purchase\Purchase;
use Captainskippah\POS\Domain\Purchase\PurchaseId;
use Captainskippah\POS\Domain\Purchase\PurchaseItem;
use Captainskippah\POS\Domain\Purchase\PurchaseRepository;
use Infrastructure\AbstractLumenRepository;
use Ramsey\Uuid\Uuid;

class LumenPurchaseRepository extends AbstractLumenRepository implements PurchaseRepository
{
    public const TABLE_NAME = 'pos_purchases';

    public const ITEMS_TABLE_NAME = 'pos_purchase_items';

    const PAYMENTS_TABLE_NAME = 'pos_purchase_payments';

    public function getAll(): array
    {
        $purchases = $this->query()->get();

        $purchaseIds = [];

        foreach ($purchases as $purchase) {
            $purchaseIds[] = $purchase->id;
        }

        $purchaseItems = $this->itemQuery()->whereIn('purchase_id', $purchaseIds)->get();

        $keyedPurchaseItems = [];

        foreach ($purchaseItems as $purchaseItem) {
            $keyedPurchaseItems[$purchaseItem->purchase_id][] = $purchaseItem;
        }

        $payments = $this->paymentQuery()->whereIn('purchase_id', $purchaseIds)->get();

        $keyedPayments = [];

        foreach ($payments as $payment) {
            $keyedPayments[$payment->purchase_id] = $payment;
        }

        $models = [];

        foreach ($purchases as $purchase) {
            $models[] = $this->queryResultToModel(
                $purchase,
                $keyedPurchaseItems[$purchase->id],
                $keyedPayments[$purchase->id]
            );
        }

        return $models;
    }

    public function getById(PurchaseId $purchaseId): ?Purchase
    {
        $purchase = $this->query()->where('id', $purchaseId->value())->first();

        if ($purchase === null) {
            return null;
        }

        $purchaseItems = $this->itemQuery()
            ->where('purchase_id', $purchaseId->value())
            ->get()
            ->all();

        $payment = $this->paymentQuery()->where('purchase_id', $purchaseId->value())->first();

        return $this->queryResultToModel($purchase, $purchaseItems, $payment);
    }

    public function save(Purchase $purchase)
    {
        $this->query()
            ->insert([
                'id' => $purchase->id()->value(),
                'vat_rate' => $purchase->vatRate(),
            ]);

        $items = [];

        foreach ($purchase->purchaseItems() as $item) {
            $items[] = [
                'purchase_id' => $purchase->id()->value(),
                'name' => $item->name(),
                'price' => $item->price(),
                'qty' => $item->quantity()
            ];
        }

        $this->itemQuery()->insert($items);

        if ($purchase->payment() !== null) {
            if ($purchase->payment()->type() === 'cash') {
                /** @var CashPayment $cashPayment */
                $cashPayment = $purchase->payment();

                $this->paymentQuery()
                    ->insert([
                        'purchase_id' => $purchase->id()->value(),
                        'amount' => $cashPayment->amount()
                    ]);
            }
        }
    }

    public function nextIdentity(): PurchaseId
    {
        return new PurchaseId(Uuid::uuid4()->serialize());
    }

    private function query()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    private function itemQuery()
    {
        return $this->database->table(self::ITEMS_TABLE_NAME);
    }

    private function paymentQuery()
    {
        return $this->database->table(self::PAYMENTS_TABLE_NAME);
    }

    private function queryResultToModel($purchase, array $purchaseItems, $payment)
    {
        $purchaseItemModels = [];

        foreach ($purchaseItems as $item) {
            $purchaseItemModels[] = new PurchaseItem($item->name, $item->price, $item->qty);
        }

        $paymentModel = null;

        if (isset($payment->amount)) {
            $paymentModel = new CashPayment($payment->amount);
        }

        $purchaseModel = new Purchase(
            new PurchaseId($purchase->id),
            $purchaseItemModels,
            $purchase->vat_rate,
            $paymentModel
        );

        return $purchaseModel;
    }
}
