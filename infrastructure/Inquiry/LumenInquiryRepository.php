<?php

namespace Infrastructure\Inquiry;

use App\Inquiry\Inquiry;
use App\Inquiry\InquiryId;
use App\Inquiry\InquiryRepository;
use Infrastructure\AbstractLumenRepository;

class LumenInquiryRepository extends AbstractLumenRepository implements InquiryRepository
{
    public const TABLE_NAME = 'inquiries';

    public function getAll(): array
    {
        return $this->query()
            ->get()
            ->map(function ($inquiry) {
                return $this->queryResultToModel($inquiry);
            })
            ->all();
    }

    public function getById(InquiryId $inquiryId): ?Inquiry
    {
        $inquiry = $this->query()
            ->where('id', $inquiryId->value())
            ->first();

        if ($inquiry === null) {
            return null;
        }

        return $this->queryResultToModel($inquiry);
    }

    public function save(Inquiry $inquiry)
    {
        if ($this->query()->where('id', $inquiry->id()->value())->exists()) {
            $this->query()
                ->where('id', $inquiry->id()->value())
                ->update([
                    'sender' => $inquiry->sender(),
                    'message' => $inquiry->message(),
                    'read' => $inquiry->isRead(),
                    'datetime' => $inquiry->date(DATE_ATOM)
                ]);
        } else {
            $this->query()
                ->insert([
                    'id' => $inquiry->id()->value(),
                    'sender' => $inquiry->sender(),
                    'message' => $inquiry->message(),
                    'read' => $inquiry->isRead(),
                    'datetime' => $inquiry->date(DATE_ATOM)
                ]);
        }
    }

    public function nextIdentity(): InquiryId
    {
        $id = $this->database
            ->selectOne(sprintf('SELECT nextval(\'%s_id_seq\')', self::TABLE_NAME))
            ->nextval;

        return new InquiryId($id);
    }

    private function query()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    private function queryResultToModel($inquiry)
    {
        $model = new Inquiry(
            new InquiryId($inquiry->id),
            $inquiry->sender,
            $inquiry->message
        );

        if ($inquiry->read) {
            $model->markAsRead();
        }

        $reflection = new \ReflectionProperty(Inquiry::class, 'date');
        $reflection->setAccessible(true);

        $datetime = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $inquiry->datetime);
        $reflection->setValue($model, $datetime);

        return $model;
    }
}
