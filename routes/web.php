<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/** @var \Laravel\Lumen\Routing\Router $router */

$router->post('auth/login', 'AuthController@login');
$router->post('inquiries', 'InquiryController@store');
$router->get('products', 'ProductController@index');
$router->get('services', 'ServiceController@index');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->group(['prefix' => 'inquiries'], function () use ($router) {
        $router->get('', 'InquiryController@index');

        $router->put('{id}/read', 'InquiryController@read');
        $router->put('{id}/unread', 'InquiryController@unread');
    });

    $router->group(['prefix' => 'products'], function () use ($router) {
        $router->post('', 'ProductController@store');

        $router->get('{id}', 'ProductController@show');
        $router->put('{id}', 'ProductController@update');
        $router->delete('{id}', 'ProductController@delete');

        $router->post('{id}/restock', 'ProductController@restock');
        $router->post('{id}/consume', 'ProductController@consume');
    });

    $router->group(['prefix' => 'services'], function () use ($router) {
        $router->post('', 'ServiceController@store');

        $router->get('{id}', 'ServiceController@show');
        $router->put('{id}', 'ServiceController@update');
        $router->delete('{id}', 'ServiceController@delete');
    });

    $router->group(['prefix' => 'pos'], function () use ($router) {
        $router->get('settings', 'PosController@settings');
        $router->post('checkout', 'PosController@checkout');
    });

    $router->group(['prefix' => 'purchases'], function () use ($router) {
        $router->get('', 'PurchaseController@index');
        $router->get('{id}', 'PurchaseController@show');
    });

    $router->group(['prefix' => 'clients'], function () use ($router) {
        $router->get('', 'ClientController@index');
        $router->post('', 'ClientController@store');

        $router->get('{id}', 'ClientController@show');
        $router->put('{id}', 'ClientController@update');
        $router->delete('{id}', 'ClientController@delete');

        $router->post('{id}/generateAccount', 'ClientController@generateAccount');

        // Doesn't make sense in getting all pets
        // Our app prefers getting by client
        $router->get('{id}/pets', 'PetController@index');
        $router->post('{id}/pets', 'PetController@store');
    });

    $router->group(['prefix' => 'pets'], function () use ($router) {
        $router->get('search', 'PetController@search');

        $router->get('{id}', 'PetController@show');
        $router->put('{id}', 'PetController@update');
        $router->delete('{id}', 'PetController@delete');

        // Doesn't make sense in getting all activities
        // Our app prefers getting by pets
        $router->get('{id}/activities', 'ActivityController@index');
        $router->post('{id}/activities', 'ActivityController@store');
    });

    $router->group(['prefix' => 'activities'], function () use ($router) {
        $router->get('{id}', 'ActivityController@show');
        $router->put('{id}', 'ActivityController@update');
        $router->delete('{id}', 'ActivityController@delete');
    });

    $router->group(['prefix' => 'calendar'], function () use ($router) {
        $router->get('{year}/{month}', 'AppointmentController@getByMonth');
        $router->post('{year}/{month}/{day}/appointments', 'AppointmentController@store');
    });

    $router->group(['prefix' => 'appointments'], function () use ($router) {
        $router->get('{id}', 'AppointmentController@getById');
        $router->delete('{id}', 'AppointmentController@delete');

        $router->put('{id}/cancel', 'AppointmentController@cancel');
        $router->put('{id}/done', 'AppointmentController@done');
        $router->put('{id}/schedule', 'AppointmentController@schedule');
    });

    $router->group(['prefix' => 'auth'], function () use ($router) {
        $router->post('logout', 'AuthController@logout');
    });
});
